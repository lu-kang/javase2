package com.itheima.d3_interface.demo2;

/**
 * 目标：接口和抽象类的区别
 * <p>
 * 一个类只能继承一个抽象类，一个类可以实现多个接口
 * 接口可以继承多个接口(接口可以多继承)
 */
public class Demo extends A implements B, C, D {

    public static void main(String[] args) {

    }
}
