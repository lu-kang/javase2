package com.itheima.d3_interface.demo3;

public class A implements B {
    @Override
    public void eat() {

    }

    @Override
    public void m1() {
        B.super.m1();
        B.m2();
    }
}
