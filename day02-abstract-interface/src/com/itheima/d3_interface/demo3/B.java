package com.itheima.d3_interface.demo3;

public interface B {

    void eat();

    default void m1() {
        System.out.println("默认方法");
    }

    static void m2() {
        System.out.println("静态方法");
    }

    private void m3() {
        System.out.println("私有方法");
    }
}
