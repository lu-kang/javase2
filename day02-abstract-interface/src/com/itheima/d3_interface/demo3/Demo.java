package com.itheima.d3_interface.demo3;

/**
 * 目标：JDK8对接口新增的功能
 * <p>
 * 1. 在JDK8版本以前，接口中只允许定义常量和抽象方法
 * 2. 从JDK8版本开始，接口中允许定义默认方法、静态方法、私有方法
 */
public class Demo {
    public static void main(String[] args) {

    }
}
