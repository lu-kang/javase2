package com.itheima.d3_interface.demo1;

/**
 * 目标：接口的定义、接口的基本使用
 * <p>
 * 1. 定义一个Animal接口，有两个方法eat、speak
 * 2. 定义一个Dog类，实现Animal接口
 */
public class Demo {

    public static void main(String[] args) {
        Dog dog =new Dog();
        dog.eat();
    }
}
