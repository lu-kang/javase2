package com.itheima.d3_interface.demo1;

public interface Animal {

    void eat();
    void speak();
}
