package com.itheima.d3_interface.demo1;

public class Dog implements Animal {

    @Override
    public void eat() {
        System.out.println("狗吃骨头");
    }

    @Override
    public void speak() {
        System.out.println("汪汪汪");
    }
}
