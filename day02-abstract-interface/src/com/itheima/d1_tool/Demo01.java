package com.itheima.d1_tool;

/**
 * 目标：掌握工具类的制作和使用
 */
public class Demo01 {
    public static void main(String[] args) {
        //定义一个整型数组
        int[] arr = {33, 12, 22, 50};

        //获取数组中的最大值
        int max = ArrayTool.getMax(arr);
        System.out.println(max);

        //对数组中的元素求和
        int sum = ArrayTool.getSum(arr);
        System.out.println(sum);
    }
}
