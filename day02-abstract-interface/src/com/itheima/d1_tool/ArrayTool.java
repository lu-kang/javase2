package com.itheima.d1_tool;

/**
 * 这个类是一个操作数组的工具类
 */
public class ArrayTool {

    /**
     * 作用：求整型数组中的最大值
     *
     * @param arr 整型数组
     * @return 返回值数组中的最大值
     */
    public static int getMax(int[] arr) {
        int max = arr[0];
        for (int i = 0; i < arr.length; ++i) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }

    /**
     * 作用：求整型数组中的元素之和
     *
     * @param arr 整型数组
     * @return 数组中的元素之和
     */
    public static int getSum(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; ++i) {
            sum += arr[i];
        }
        return sum;
    }
}
