package com.itheima.d1_tool;

/**
 * 目标：掌握Math工具类的使用
 */
public class Demo02 {
    public static void main(String[] args) {
        //static double ceil(double a) 向上取整
        //static double floor(double a) 向下取整
        //static int max(int a, int b) 返回两个int中的较大值
        //static int min(int a, int b) 返回两个 int中的较小值
        //static double random() 返回大于等于0.0，小于1.0的随机数
        //static long round(double a) 返回a的四舍五入值

        System.out.println(Math.ceil(3.14));
        System.out.println(Math.floor(3.14));
        System.out.println(Math.max(3, 10));
        System.out.println(Math.min(3, 10));
        System.out.println(Math.random());
        System.out.println(Math.round(3.14));
    }
}
