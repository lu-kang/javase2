package com.itheima.d2_abstract.demo2;

/**
 * 抽象类：动物类
 */
public abstract class Animal {

    //定义吃的方法
    public abstract void eat();
}
