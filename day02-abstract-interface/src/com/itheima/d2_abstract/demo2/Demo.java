package com.itheima.d2_abstract.demo2;

/**
 * 目标：掌握抽象类的使用
 * <p>
 * 抽象类：Animal
 * 实现类：Cat、Dog
 */
public class Demo {
    public static void main(String[] args) {

        //注意：我们不能直接创建抽象类对象，必须创建子类对象使用
        //Animal animal=new Animal();

        Cat cat = new Cat();
        cat.eat();

    }
}
