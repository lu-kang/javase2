package com.itheima.d2_abstract.demo1;

public class Fu {

    //public 公共
    public void m1() {
        System.out.println("m1...");
    }

    //private 私有
    private void m2() {
        System.out.println("m1...");
    }

    //protected 受保护
    protected void m3() {
        System.out.println("m1...");
    }

    //缺省
    void m4() {
        System.out.println("m1...");
    }
}
