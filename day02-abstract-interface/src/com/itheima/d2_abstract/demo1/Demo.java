package com.itheima.d2_abstract.demo1;


/**
 * 目标：掌握四种权限修饰符的访问范围
 * <p>
 * 修饰符     同一个类中       同一包中其他类     不同包下子类      不同包下的无关类
 * private     √
 * 缺省         √                √
 * protected   √                √                 √
 * public      √                √                 √                √
 * <p>
 * public：都可以访问
 * private：只能在本类中访问
 * protected：都可以访问（既不同包又非子类除外）
 * 缺省：同包即可
 */
public class Demo {

    public static void main(String[] args) {
        Fu f = new Fu();
    }
}
