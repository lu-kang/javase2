package com.itheima.d2_abstract.demo3;

/**
 * 目标：抽象类的应用（模板方法模式）
 * <p>
 * 1. 定义一个作文模板类Template，提供write写作文的方法，在write方法中提供固定的作文开头和固定的作为结尾，中间body内容由子类自己完成。
 * 2. 定义一个Anna类，继承Template，实现body方法
 * 3. 定义一个Tom类，继承Template，实现body方法
 */
public class Demo {
    public static void main(String[] args) {
        //1. 创建子类对象
        //2. 调用write写作文的方法
        Tom tom = new Tom();
        Anna anna = new Anna();

        tom.write();
        anna.write();
    }
}
