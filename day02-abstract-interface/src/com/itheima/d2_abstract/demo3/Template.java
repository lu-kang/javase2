package com.itheima.d2_abstract.demo3;

public abstract class Template {

    public abstract void body();

    public void write() {
        System.out.println("早晨太阳阳光明媚，我出门了。");
        body();
        System.out.println("晚上夕阳余晖落下，我回家了。");
    }

}
