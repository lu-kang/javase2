package com.itheima.d4_polymorphic.demo2;

public class Dog extends Animal {

    //重写父类吃的方法
    @Override
    public void eat() {
        System.out.println("狗吃骨头");
    }

    //定义看门的方法
    public void watchDoor() {
        System.out.println("狗看们");
    }
}
