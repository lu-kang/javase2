package com.itheima.d4_polymorphic.demo2;

/**
 * 动物园
 */
public class Zoo {

    //接收一个动物
    public void receive(Animal a) {
        a.eat();
    }

}
