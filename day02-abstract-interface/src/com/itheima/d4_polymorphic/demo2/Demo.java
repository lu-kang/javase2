package com.itheima.d4_polymorphic.demo2;

/**
 * 目标：多态的好处和弊端以及多态转型
 */
public class Demo {
    public static void main(String[] args) {
        //1. 创建动物园对象
        Zoo zoo = new Zoo();

        //2. 动物园接收动物
        Animal animal = new Cat();
        zoo.receive(animal);

    }
}
