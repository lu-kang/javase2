package com.itheima.d4_polymorphic.demo1;

/**
 * 目标：多态的表现形式
 * <p>
 * 父类 变量 = new 子类();
 */
public class Demo {
    public static void main(String[] args) {

            Animal a1 = new Dog();
            Animal a2 = new Cat();

            a1.eat();
            a2.eat();

    }
}
