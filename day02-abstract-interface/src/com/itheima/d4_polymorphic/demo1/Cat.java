package com.itheima.d4_polymorphic.demo1;

public class Cat extends Animal {

    //重写父类吃的方法
    @Override
    public void eat() {
        System.out.println("猫吃鱼");
    }

    //定义抓老鼠的方法
    public void catchMouse() {
        System.out.println("猫抓老鼠");
    }
}
