package com.itheima.d4_polymorphic.demo3;

// 1 定义USB接口,提供接入,工作,拔出
public interface USB {

    //接入
    void start();

    //工作
    void work();

    //拔出
    void end();
}
