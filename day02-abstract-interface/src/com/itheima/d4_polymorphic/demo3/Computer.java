package com.itheima.d4_polymorphic.demo3;

// 定义电脑类,在电脑类中定义install安装的方法,接收USB设备
public class Computer {

    public void install(USB usb) {  //将来调用方法可以传递Mouse和Keyboard对象
        usb.start();    //接入
        usb.work();     //工作
        usb.end();      //拔出
    }
}
