package com.itheima.d4_polymorphic.demo3;

/**
 * 键盘类
 */
public class Keyboard implements USB {
    @Override
    public void start() {
        System.out.println("键盘插入");
    }

    @Override
    public void work() {
        System.out.println("键盘输入");
    }

    @Override
    public void end() {
        System.out.println("键盘拔出");
    }
}
