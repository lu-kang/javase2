package com.itheima.d4_polymorphic.demo3;

/**
 * 目标：多态的综合案例
 * 需求：
 * 使用面向对象编程模拟：设计一个电脑对象(Computer)，可以安装(Install)2个USB设备
 * 鼠标(Mouse)：被安装时可以完成接入、调用点击功能、拔出功能。
 * 键盘(Keyboard)：被安装时可以完成接入、调用打字功能、拔出功能。
 * <p>
 * 分析:
 * 1 定义USB接口,提供接入,工作,拔出
 * 2 定义键盘,鼠标实现类,重写以上方法
 * 3 定义电脑类,在电脑类中定义install安装的方法,接收USB设备
 * 4 在测试类中创建电脑对象,调用install方法
 */
public class Demo {
    public static void main(String[] args) {
        // 创建电脑对象,调用install方法
        Computer computer = new Computer();

        //安装鼠标
        computer.install(new Mouse());
        //安装键盘
        computer.install(new Keyboard());
    }
}
