package com.itheima.d4_polymorphic.demo3;

public class Mouse implements USB {
    @Override
    public void start() {
        System.out.println("鼠标插入");
    }

    @Override
    public void work() {
        System.out.println("鼠标点击");
    }

    @Override
    public void end() {
        System.out.println("鼠标拔出");
    }
}
