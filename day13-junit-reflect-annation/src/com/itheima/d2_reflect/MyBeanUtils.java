package com.itheima.d2_reflect;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

public final class MyBeanUtils {
    private MyBeanUtils() {
    }

    /**
     * 作用：将map集合中的数据封装到obj对象身上
     * 要求：map集合的key和obj的属性名一样
     *
     * @param obj 要封装值的javabean对象
     * @param map 提供数据的map集合
     */
    public static void populate(Object obj, Map<String, Object> map) throws Exception {
        //1 获取obj的Class对象
        Class<?> aClass = obj.getClass();
        //2 获取map集合的key，遍历所有的key
        Set<String> keys = map.keySet();
        for (String key : keys) { //key：name、age、gender
            //3 根据key获取Class对象的Field成员变量对象
            Field field = aClass.getDeclaredField(key);
            //4 将key对应的value值赋值给field
            field.setAccessible(true);
            field.set(obj, map.get(key));  //obj.name=map.get("name")
        }
    }
}
