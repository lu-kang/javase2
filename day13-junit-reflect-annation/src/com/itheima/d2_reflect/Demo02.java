package com.itheima.d2_reflect;

import java.lang.reflect.Constructor;

/**
 * 目标：掌握利用反射获取构造器Constructor对象
 */
public class Demo02 {
    public static void main(String[] args) throws Exception {
        //1 获取Class对象
        Class<Student> clazz = Student.class;

        //2 获取构造器对象
        //2.1 Constructor<?>[] getConstructors()：返回所有构造器对象的数组（只能拿public的）
        //2.2 Constructor<?>[] getDeclaredConstructors()：返回所有构造器对象的数组，存在就能拿到
        //2.3 Constructor<T> getConstructor(Class<?>... parameterTypes)：返回单个构造器对象（只能拿public的）
        //2.4 Constructor<T> getDeclaredConstructor(Class<?>... parameterTypes)：返回单个构造器对象，存在就能拿到
        Constructor<Student> constructor = clazz.getDeclaredConstructor(String.class, int.class, String.class);

        //3 通过构造器实例化一个对象，注意：如果构造器被private修饰，默认没有权限访问（实例化），需要在访问之前开启访问权限(暴力访问)
        constructor.setAccessible(true);//暴力访问
        Student stu = constructor.newInstance("张益达", 20, "男");
        System.out.println(stu);

        //说明：如果需要实例化对象，通过Class对象可以。只是一种简单操作,只能访问public修饰的空参构造
        Student student = clazz.newInstance();
        System.out.println("student = " + student);
    }
}
