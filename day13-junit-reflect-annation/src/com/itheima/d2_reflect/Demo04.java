package com.itheima.d2_reflect;

import java.lang.reflect.Method;

/**
 * 目标：掌握利用反射获取方法Method对象
 */
public class Demo04 {
    public static void main(String[] args) throws Exception {
        //1 获取Class对象
        Class<Student> clazz = Student.class;

        //2 获取成员方法Method对象
        //2.1 Method[] getMethods()：返回所有成员方法对象的数组（只能拿public的）
        //2.2 Method[] getDeclaredMethods()：返回所有成员方法对象的数组，存在就能拿到
        //2.3 Method getMethod(String name, Class<?>... parameterTypes)：返回单个成员方法对象（只能拿public的）
        //2.4 getDeclaredMethod(String name, Class<?>... parameterTypes)：返回单个成员方法对象，存在就能拿到
        Method method = clazz.getDeclaredMethod("show");

        //实例化一个学生对象
        Student student = clazz.newInstance();

        //3 操作成员方法（反射调用方法）,如果反射调用的方法没有返回值，那么invoke方法的返回值就null。
        method.setAccessible(true);
        method.invoke(student);
    }
}
