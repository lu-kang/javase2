package com.itheima.d2_reflect;

import java.lang.reflect.Field;

/**
 * 目标：掌握利用反射获取成员变量Field对象
 */
public class Demo03 {
    public static void main(String[] args) throws Exception {
        //1 获取Class对象
        Class<Student> clazz = Student.class;

        //2 获取成员变量对象
        //2.1 Field[] getFields()：返回所有成员变量对象的数组（只能拿public的）
        //2.2 Field[] getDeclaredFields()：返回所有成员变量对象的数组，存在就能拿到
        //2.3 Field getField(String name)：返回单个成员变量对象（只能拿public的）
        //2.4 Field getDeclaredField(String name)：返回单个成员变量对象，存在就能拿到
        Field field = clazz.getDeclaredField("age");

        //实例化一个Student对象
        Student stu = clazz.newInstance();

        //3 操作成员变量（赋值、取值）
        //赋值。参数1：表示给哪个对象的属性赋值。参数2：表示给对象当前属性赋的值
        //field.set(student,"男");  //相当于:student.gender="男"

        //注意：如果成员变量被private修饰，在操作之前需要开启访问权限
        field.setAccessible(true);
        field.set(stu, 10);  //相当于:stu.age=10

        //取值
        //String value = (String) field.get(student);  //相当于:   String value = student.gender
        int value = (int) field.get(stu);  //相当于:   int age = student.age
        System.out.println(value);
    }
}
