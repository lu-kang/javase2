package com.itheima.d2_reflect;

/**
 * 目标：掌握获取Class类对象的三种方式
 */
public class Demo01 {
    public static void main(String[] args) throws Exception {
        //方式1：Class类静态方法forName(String className)
        Class<Student> clazz1 = (Class<Student>) Class.forName("com.itheima.d2_reflect.Student");
        System.out.println("clazz1 = " + clazz1);

        //方式2：类名.class
        Class<Student> clazz2 = Student.class;
        System.out.println("clazz2 = " + clazz2);
        //注意：任何类的class对象是唯一的。单例
        System.out.println(clazz1 == clazz2);

        //方式3：对象.getClass()
        Student student = new Student();
        Class<? extends Student> clazz3 = student.getClass();
        System.out.println("clazz3 = " + clazz3);
        System.out.println(clazz1 == clazz3);
    }
}
