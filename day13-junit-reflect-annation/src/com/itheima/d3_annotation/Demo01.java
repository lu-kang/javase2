package com.itheima.d3_annotation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 目标：了解JDK几个常用内置注解
 *
 * @Override：检验方法重写的正确性
 * @FunctionalInterface：检验接口是否是函数式接口
 * @Deprecated：用来标记一个方法是已过时的方法
 * @SuppressWarnings(“警告类型”)：用来压制编译器的警告。警告类型了解两个： deprecation：抑制过期方法警告；all：抑制所有警告
 */
public class Demo01 {

    @SuppressWarnings("all")
    public static void main(String[] args) {
        Date date = new Date();
        System.out.println("date.getYear() = " + date.getYear());
        List list = new ArrayList();
    }

    //@Override  //show方法不是重写的方法，父类中没有。
    public void show() {
    }


    @Override
    public String toString() {
        System.out.println("toString...");
        return "hello";
    }
}
