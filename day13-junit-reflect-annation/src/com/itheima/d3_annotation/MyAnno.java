package com.itheima.d3_annotation;

import java.lang.annotation.*;

/**
 * 自定义注解
 */
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD})//定义注解的使用位置
@Retention(RetentionPolicy.RUNTIME)  //使用最多
public @interface MyAnno {

    String name() default "";

    int age() default 0;

    String value();  //当有且仅有一个名称叫value的属性需要赋值时，在赋值的时候value名称可以省略。如果有多个属性同时赋值，value名称就不能省略。
}
