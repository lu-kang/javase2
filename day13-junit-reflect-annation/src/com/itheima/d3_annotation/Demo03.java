package com.itheima.d3_annotation;

import java.lang.reflect.Method;

/**
 * 目标：了解注解的解析
 * <p>
 * 1.通过Class对象获取类上的注解对象，再通过注解对象获取值
 * 2.通过Method对象获取方法上的注解对象，再通过注解对象获取值
 * <p>
 * Annotation[] getDeclaredAnnotations()：获得当前对象上使用的所有注解，返回注解数组
 * T getDeclaredAnnotation(Class<T> annotationClass)：根据注解类型获得对应注解对象
 * boolean isAnnotationPresent(Class<Annotation> annotationClass)：判断当前对象是否使用了指定的注解，如果使用了则返回true，否则false
 */

@MyAnno("aaa")
public class Demo03 {

    @MyAnno(value = "ccc", name = "张益达")
    public void show() {
    }

    public static void main(String[] args) throws Exception {
        //1 获取Class对象，在通过Class对象获取类上的注解对象，再通过注解对象获取值
        Class<Demo03> clazz = Demo03.class;
        MyAnno myAnno = clazz.getDeclaredAnnotation(MyAnno.class);
        System.out.println(myAnno.value());

        //2 获取Class对象，在通过Class对象获取Method方法对象，再通过Method获取方法上的注解对象，再通过注解对象获取值
        Method method = clazz.getDeclaredMethod("show");
        MyAnno anno = method.getDeclaredAnnotation(MyAnno.class);
        System.out.println(anno.name());
        System.out.println(anno.value());
    }
}