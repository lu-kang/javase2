package com.itheima.d3_annotation;

import java.lang.reflect.Method;

/**
 * 案例：定义若干个方法，只要加了MyTest注解，就可以在启动时被触发执行
 */
public class Demo04 {

    @MyTest
    public void method1() {
        System.out.println("---method1执行了---");
    }

    public void method2() {
        System.out.println("---method2执行了---");
    }

    @MyTest
    public void method3() {
        System.out.println("---method3执行了---");
    }

    public static void main(String[] args) throws Exception {
        //需求： 解析注解，有MyTest注解的方法就要执行
        //1 获取Class对象
        Class<Demo04> clazz = Demo04.class;
        Demo04 d = clazz.newInstance();
        //2 获取所有的方法对象
        Method[] methods = clazz.getDeclaredMethods();
        //3 遍历所有方法的数组
        for (Method method : methods) {
            //4 判断方法是否有@MyTest注解。如果有就执行这个方法
            if (method.isAnnotationPresent(MyTest.class)) {//返回true表示有该注解
                //执行这个方法
                method.invoke(d);
            }
        }
    }
}
