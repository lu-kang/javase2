package com.itheima.d1_junit;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * 需求：定义单元测试类，测试UserManager中的方法
 * <p>
 * 注意点：
 * 1、测试方法必须是公开的，无参数 无返回值的方法
 * 2、测试方法必须使用@Test注解标记。
 * 预期结果的正确性测试：断言Assert.assertEquals(错误的标识消息, 预期结果, 实际结果);
 * <p>
 * Junit常用注解：@Test
 */
//测试类类名规范：被测试类Test，例如：UserManagerTest
public class UserManagerTest {

    private UserManager userManager;

    @Before
    public void beforeClass() throws Exception {
        userManager = new UserManager();
    }

    /**
     * 要求：
     * 1、方法必须是 公共的，无数，无返回的非静态方法
     * 2、方法上需要使用@Test注解表示该方法是一个单元测试方法，法名规范：test被测试方法的名字()
     */
    @Test
    public void testLogin() {
        //System.out.println("testLogin...");
        boolean flag = userManager.login("admin", "666");
        System.out.println("login：flag = " + flag);
        //断言测试：仅仅适用于测试有返回值的方法
        Assert.assertEquals("用户名和密码错误", true, flag); //参数1：预期结果。参数2：实际结果，两个结果一致才绿条
        Assert.assertTrue("用户名和密码错误", flag);
    }

    /**
     * 如果导入了junit jar包并引入了junit，那么就可以使用alt+insert--->Test Method自动生成单元测试方法
     */
    @Test
    public void testSelectByUsername() {
        boolean flag = userManager.selectByUsername("zhangsan");
        System.out.println("selectByUsername：flag = " + flag);
    }

}
