package com.itheima.d4_static.demo02;

/**
 * 目标：掌握static修饰方法
 * <p>
 * 1. 被static修饰的方法叫静态方法，直接通过 类名.方法名() 使用
 * 2. 静态只能访问静态的成员变量和成员方法。
 * 3. 非静态既可以访问静态，也可以访问非静态的成员变量和成员方法。
 */
public class Demo {

    public static void main(String[] args) {
        int[] arr = {11, 22, 33};
        ArrayUtils.print(arr);
    }
}
