package com.itheima.d4_static.demo03;

public class Student {

    static {
        System.out.println("静态代码块，只执行一次");
    }

    {
        System.out.println("构造代码块，每次在构造方法前执行");
    }

    public Student() {
        System.out.println("构造方法");
    }

}
