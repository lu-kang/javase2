package com.itheima.d4_static.demo03;

/**
 * 目标：了解static修饰代码块
 * <p>
 * 静态代码块：格式：static{}    特点：类加载的时候就执行，只执行一次
 * 构造代码块：格式：{}          特点：在构造方法调用前执行，每次调用构造方法都会执行一次
 */
public class Demo {
    public static void main(String[] args) {

        Student s1 = new Student();
        Student s2 = new Student();
    }
}
