package com.itheima.d4_static.demo01;

public class Student {

    //静态成员变量，也叫类变量，该类的所有对象共享
    static String school;

    //实例成员变量，也就是我们通常说的成员变量
    String name;

}
