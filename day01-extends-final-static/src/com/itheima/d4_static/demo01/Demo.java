package com.itheima.d4_static.demo01;

/**
 * 目标：掌握static修饰成员变量
 * <p>
 * 被static修饰的变量叫静态变量，访问直接通过 类名.变量名
 */
public class Demo {

    public static void main(String[] args) {
        Student.school = "黑马程序员";

        Student s1 = new Student();
        Student s2 = new Student();

        s1.name = "zs";
        s2.name = "ls";

        System.out.println(MessageConstants.VERIFY_CODE_ERROR);

    }
}


