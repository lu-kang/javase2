package com.itheima.d4_static.demo04;

/**
 * 产品经理  继承  员工类
 */
public class Manager extends Employee {

    public Manager() {
    }

    public Manager(String name, int age, String deptName) {
        super(name, age, deptName);
    }

    //重写work方法
    @Override
    public void work() {
        System.out.println("产品经理工作职责是设计产品原型");
    }
}
