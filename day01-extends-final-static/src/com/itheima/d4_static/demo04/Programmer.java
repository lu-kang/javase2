package com.itheima.d4_static.demo04;

/**
 * 程序员类 继承 员工类
 */
public class Programmer extends Employee {

    public Programmer() {
    }

    public Programmer(String name, int age, String deptName) {
        super(name, age, deptName);
    }

    @Override
    public void work() {
        System.out.println("程序员工作职责是使劲敲代码");
    }
}
