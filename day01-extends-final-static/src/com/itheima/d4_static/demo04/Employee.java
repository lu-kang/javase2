package com.itheima.d4_static.demo04;

/**
 * 员工类
 * 1.程序员和产品经理都是员工的一种，可以将共性内容抽取成一个员工类。
 * 2.员工类的属性：姓名、年龄、部门、公司名称（静态、常量）
 * 3.员工类的行为：work()方法（但是具体做什么不确定）
 */
public class Employee {

    public static final String companyName = "黑马头条科技有限公司";  //静态常量

    private String name;    //姓名
    private int age;    //年龄
    private String deptName;    //部门名称

    public Employee() {
    }

    public Employee(String name, int age, String deptName) {
        this.name = name;
        this.age = age;
        this.deptName = deptName;
    }

    //员工类的行为：work()方法（但是具体做什么不确定）
    public void work() {
    }

    //生成get和set方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }
}
