package com.itheima.d4_static.demo04;

/**
 * 需求：
 * 1. 在黑马头条科技有限公司，员工分为产品经理和程序员两种；
 * 2. 他们都有姓名、年龄、部门名称、公司名称等属性；还有工作的行为；--->使用抽象父类
 * 3. 产品经理的工作职责设计产品原型，程序员的工作职责是使劲敲代码。--->工作方法应该抽象，子类重写
 * <p>
 * <p>
 * 分析：
 * 1. 程序员和产品经理都是员工的一种，可以将共性内容抽取成一个员工类。
 * 2. 员工类的属性：姓名、年龄、部门、公司名称（静态、常量）
 * 3. 员工类的行为：work()方法（但是具体做什么不确定）
 * 4. 程序员类、产品经理都继承员工类，重写work()方法
 */
public class Demo {
    public static void main(String[] args) {

        //1. 创建程序员对象
        Programmer programmer = new Programmer("张三", 20, "研发部");

        //2. 打印程序员信息
        System.out.println("我是" + Employee.companyName + "的员工");
        System.out.println("我的姓名是：" + programmer.getName());
        System.out.println("我的年龄是：" + programmer.getAge());
        System.out.println("我的部门是：" + programmer.getDeptName());
        //3. 调用工作方法
        programmer.work();

        System.out.println("-------------------");

        //创建产品经理对象，打印产品经理信息，调用工作方法

    }
}
