package com.itheima.d3_final;

/**
 * 目标：掌握final关键字的特点
 * <p>
 * 修饰变量：也叫常量。变量存储的值不能被修改
 * 修饰方法：表明该方法是最终方法，不能被重写
 * 修饰类：表明该类是最终类，不能被继承
 * <p>
 * final修饰基本数据类型时，不变的是变量存储的数据值
 * final修饰引用数据类型时，不变的是变量存储的地址值
 */
public class Demo {

    public static void main(String[] args) {
        /*final*/ int a = 10;

        /*final*/ Zi zi = new Zi();

    }
}
