package com.itheima.d2_extends.demo06;

/**
 * 目标：继承后：访问父类的构造器
 * <p>
 * 特点：调用子类的构造器，默认都会先访问父类的空参构造器
 */
public class Demo {
    public static void main(String[] args) {
        Zi z1 = new Zi();
    }
}
