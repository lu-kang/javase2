package com.itheima.d2_extends.demo06;

/**
 * 子类
 */
public class Zi extends Fu {

    String name;

    public Zi() {
        super("zi");
        System.out.println("子类无参构造");
    }

}
