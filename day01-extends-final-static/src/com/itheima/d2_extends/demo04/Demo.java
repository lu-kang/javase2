package com.itheima.d2_extends.demo04;

/**
 * 目标：继承后：访问父类的属性、和方法
 */
public class Demo {
    public static void main(String[] args) {

        //1. 创建子类对象
        Zi zi = new Zi();
        //2. 调用子类print()方法
        zi.print();

    }
}
