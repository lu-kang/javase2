package com.itheima.d2_extends.demo04;

public class Fu {

    String name = "父类";
    int age = 42;
    String city = "中国";

    public void drive() {
        System.out.println("Fu：驾驶AE86在秋名山上连续过弯！");
    }
}
