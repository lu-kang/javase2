package com.itheima.d2_extends.demo04;

public class Zi extends Fu {

    String name = "子类";
    int age = 23;

    public void drive() {
        System.out.println("Zi：驾驶兰博基尼在城市的灯红酒绿间穿梭！");
    }

    public void print() {
        String name = "方法内变量";
        //TODO 如何访问子类的属性、和方法
        System.out.println(name);
        System.out.println(this.name);
        this.drive();

        //TODO 如何访问父类的属性、和方法
        System.out.println(super.name);
        super.drive();

    }
}
