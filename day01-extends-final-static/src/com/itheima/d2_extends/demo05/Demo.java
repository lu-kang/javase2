package com.itheima.d2_extends.demo05;

/**
 * 目标：继承后：重写父类的方法
 * <p>
 * 父类：Phone 有 call 方法
 * 子类：HWPhone、MiPhone、IPhone
 */
public class Demo {

    public static void main(String[] args) {
        HWPhone hwPhone = new HWPhone();
        hwPhone.call();
    }
}
