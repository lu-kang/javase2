package com.itheima.d2_extends.demo02;

/**
 * 案例：在传智教育的tlias教学资源管理系统中，存在学生、老师这两个角色。
 * <p>
 * 学生：属性（名称，年龄，所在班级className），方法（查看课表lookCourse()，填写听课反馈feedback()）
 * 老师：属性（名称，年龄，部门名称deptName），方法（查看课表lookCourse()，发布问题release()）
 * <p>
 * 提示：相同的信息定义到父类(Person)中，然后让Student类和Teacher类继承Person类
 */
public class Demo {

    public static void main(String[] args) {
        Student s = new Student();
        Teacher t = new Teacher();

        s.feedback();
        t.lookCourse();

    }
}
