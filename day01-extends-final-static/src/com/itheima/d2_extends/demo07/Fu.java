package com.itheima.d2_extends.demo07;

/**
 * 父类
 */
public class Fu {

    String name;

    public Fu() {
    }

    public Fu(String name) {
        this.name = name;
    }
}
