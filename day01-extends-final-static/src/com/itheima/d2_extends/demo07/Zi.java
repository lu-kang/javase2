package com.itheima.d2_extends.demo07;

/**
 * 子类
 */
public class Zi extends Fu {

    String name;

    public Zi() {
    }

    public Zi(String name) {
        this.name = name;
    }
}
