package com.itheima.d2_extends.demo07;

/**
 * 目标：继承后：this、super关键字
 * <p>
 * this：代表本类对象的引用。
 * super：代表父类对象的引用。
 */
public class Demo {
    public static void main(String[] args) {
        Zi z = new Zi("张无忌");
        System.out.println(z.name);
    }
}
