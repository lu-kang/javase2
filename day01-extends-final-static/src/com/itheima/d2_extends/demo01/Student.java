package com.itheima.d2_extends.demo01;

/**
 * 学生类
 */
public class Student extends Person {

    public void study() {
        System.out.println("好好学习，天天学习");
    }
}
