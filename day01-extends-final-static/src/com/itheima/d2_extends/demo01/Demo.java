package com.itheima.d2_extends.demo01;

/**
 * 目标：学习继承的格式，提高代码复用性
 * <p>
 * 继承实现格式：public class 子类 extends 父类 { }
 */
public class Demo {

    public static void main(String[] args) {
        //Student、Teacher中有重复的内容，我们可以新定义一个类，将相同的内容放到该类中。让Student、Teacher继承该类。
        Student s = new Student();
        s.study();

    }
}