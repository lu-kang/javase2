package com.itheima.d2_extends.demo01;

//人类：抽取父类
public class Person {

    private String name;
    private int age;

    //以下是get、set方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

}
