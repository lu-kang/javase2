package com.itheima.d2_extends.demo03;

/**
 * 目标：掌握继承的注意事项
 * <p>
 * 注意事项：
 * 1. Java只能单继承，不能多继承，但可以多层继承
 * 2. Java中任何一个类都直接或者间接的继承自Object类
 */
public class Demo {

    public static void main(String[] args) {
        //1. 定义三个类：A，B，C
        //2. 让A继承B，B继承C

    }
}
