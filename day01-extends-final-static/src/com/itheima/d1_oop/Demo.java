package com.itheima.d1_oop;

/**
 * 目标：复习对象的创建和使用
 * <p>
 * 步骤1：创建Teacher类
 * 步骤2：通过new关键字，创建对象
 */
public class Demo {
    public static void main(String[] args) {

        Teacher teacher = new Teacher();
        System.out.println(teacher);
    }
}
