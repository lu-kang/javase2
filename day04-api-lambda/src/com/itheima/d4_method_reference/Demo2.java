package com.itheima.d4_method_reference;

import com.itheima.d3_lambda.Student;

import java.util.Arrays;

/**
 * 目标：掌握实例方法的引用
 * <p>
 * 使用场景：如果某个Lambda表达式里只是调用一个实例方法，并且前后参数的形式一致，就可以使用实例方法引用
 * 引用格式：对象名::方法名
 */
public class Demo2 {
    public static void main(String[] args) {
        Student[] arr = new Student[4];
        arr[0] = new Student("蜘蛛精", 169.5, 23);
        arr[1] = new Student("紫霞", 163.8, 26);
        arr[2] = new Student("紫霞", 163.8, 26);
        arr[3] = new Student("至尊宝", 167.5, 24);

        //使用Lambda表达式对students数组中的学生按照身高的升序排列（要求：调用CompareByData类的compareByHeight方法）
        Arrays.sort(arr, new CompareByData()::compareByHeight);

    }
}

