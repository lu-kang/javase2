package com.itheima.d4_method_reference;

import com.itheima.d3_lambda.Student;

public class CompareByData {

    //静态方法
    public static int compareByAge(Student o1, Student o2) {
        return o1.getAge() - o2.getAge();
    }

    //实例方法
    public int compareByHeight(Student o1, Student o2) {
        return Double.compare(o1.getHeight(), o2.getHeight());
    }
}
