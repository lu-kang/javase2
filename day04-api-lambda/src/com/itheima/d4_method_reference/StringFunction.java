package com.itheima.d4_method_reference;

public interface StringFunction {

    String create(char[] chars);
}
