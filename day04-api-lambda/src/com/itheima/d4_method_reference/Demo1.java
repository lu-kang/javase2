package com.itheima.d4_method_reference;

import com.itheima.d3_lambda.Student;

import java.util.Arrays;

/**
 * 目标：掌握静态方法的引用
 * <p>
 * 使用场景：如果某个Lambda表达式里只是调用一个静态方法，并且前后参数的形式一致，就可以使用静态方法引用
 * 引用格式：类名::方法名
 */
public class Demo1 {
    public static void main(String[] args) {
        Student[] arr = {
                new Student("蜘蛛精", 169.5, 23),
                new Student("紫霞", 163.8, 23),
                new Student("牛魔王", 170.8, 28),
                new Student("至尊宝", 167.5, 24)
        };
        //使用Lambda表达式对数组中的学生按照年龄的升序排列，要求：调用CompareByData类的compareByAge方法
        Arrays.sort(arr, CompareByData::compareByAge);

    }
}

