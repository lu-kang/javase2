package com.itheima.d4_method_reference;

/**
 * 目标：掌握构造器的引用
 * <p>
 * 使用场景：如果某个Lambda表达式里只是在创建对象，并且前后参数情况一致，就可以使用构造器引用。
 * 引用格式：类名::new
 */
public class Demo4 {
    public static void main(String[] args) {

        //1.创建StringFunction接口的匿名内部类对象
        StringFunction f1 = new StringFunction() {
            @Override
            public String create(char[] chars) {
                return new String(chars);
            }
        };

        //2.使用Lambda类改进
        StringFunction f2 = chars -> new String(chars);

        //3.使用方法引用改进：构造器引用
        StringFunction f3 = String::new;
    }


}
