package com.itheima.d4_method_reference;

import java.util.Arrays;

/**
 * 目标：掌握特定类型的方法引用
 * <p>
 * 使用场景：如果某个Lambda表达式里只是调用一个实例方法，并且前面参数列表中的第一个参数是作为方法的主调，后面的所有参数都是作为该实例方法的入参，则此时也可以使用类名进行引用。
 * 引用格式：类名::方法名
 */
public class Demo3 {
    public static void main(String[] args) {
        //要求忽略字符大小写进行排序
        String[] arr = {"body", "angela", "andy", "dei", "coco", "aib", "jack", "cik"};

        //1.lambda表达式写法
        Arrays.sort(arr, (o1, o2) -> o1.compareTo(o2));

        //2.方法引用写法
        Arrays.sort(arr, String::compareTo);
    }
}
