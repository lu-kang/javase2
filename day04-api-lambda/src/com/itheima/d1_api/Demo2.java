package com.itheima.d1_api;

import java.util.Date;

/**
 * 练习：请计算出当前时间往后走1小时26分之后的时间是多少？
 */
public class Demo2 {
    public static void main(String[] args) {

        long time = new Date().getTime() + 86 * 60 * 1000;
        Date date = new Date(time);
        System.out.println(date);

    }
}
