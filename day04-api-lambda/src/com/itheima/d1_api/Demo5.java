package com.itheima.d1_api;

import java.util.Arrays;

/**
 * 目标：掌握Arrays工具类的常用方法
 */
public class Demo5 {
    public static void main(String[] args) {
        /*
            public static String toString(int[] arr) 返回指定数组内容的字符串表示形式
            public static void sort(int[] arr) 对指定的 int 型数组按数字升序进行排序。
            public static int binarySearch(int[] arr, int key) 使用二分搜索法来搜索指定的 int 型数组，以获得指定的值。
         */
        int[] arr = {50, 30, 20, 40, 10, 21, 33};

        System.out.println(Arrays.toString(arr));

        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));

        System.out.println(Arrays.binarySearch(arr, 33));
    }
}
