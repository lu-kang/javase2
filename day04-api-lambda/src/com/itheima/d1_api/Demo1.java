package com.itheima.d1_api;

import java.util.Date;

/**
 * 目标：掌握Date日期类的使用
 */
public class Demo1 {
    public static void main(String[] args) {

        /*
            public Date()                       创建Date对象，代表系统当前日期时间
            public Date(long time)              创建Date对象，代表自1970年1月1日以来的日期时间

            public long getTime()               返回从1970年1月1日 0时0分0秒以来的毫秒值
            public void setTime(long time)      设置Date对象自1970年1月1日 0时0分0秒以来的毫秒值
         */

        Date date = new Date();
        System.out.println(date);

        date.setTime(0);
        System.out.println(date.getTime());



    }
}
