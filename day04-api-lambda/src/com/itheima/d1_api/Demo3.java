package com.itheima.d1_api;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * 目标：掌握SimpleDateFormat日期格式类
 * <p>
 * 常见日期格式：yyyy-MM-dd HH:mm:ss
 */
public class Demo3 {
    public static void main(String[] args) throws ParseException {
        //按指定格式输出当前日期
        //1.先创建SimpleDateFormat对象，并指定日期格式
        //2.格式化：将Date对象转换为指定格式的日期字符串
        //3.解析：将指定格式的字符串解析为Date对象
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        System.out.println(dateFormat.format(new Date()));

        Date date = dateFormat.parse("1999-09-09 00:00:00");
        System.out.println(date);
    }
}
