package com.itheima.d1_api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 练习：请计算出 2023年01月21日0点0分0秒，往后走2天14小时后的时间是多少？
 */
public class Demo4 {
    public static void main(String[] args) throws ParseException {
        String str = "2023年01月21日0点0分0秒";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日HH点mm分ss秒");
        Date date = dateFormat.parse(str);

        Date newDate = new Date(date.getTime() + (24 * 2 + 14) * 60 * 60 * 1000);
        System.out.println(dateFormat.format(newDate));

    }
}
