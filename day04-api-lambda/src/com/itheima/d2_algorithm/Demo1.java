package com.itheima.d2_algorithm;

import java.util.Arrays;

/**
 * 目标：演示冒泡排序的算法
 * <p>
 * 排序的规律：
 * 1.每一轮都是从0索引开始，把两个相邻的元素进行比较
 * 2.每一轮比较的次数，比上一轮少一次
 * 3.比较的轮数 = 长度-1
 */
public class Demo1 {

    public static void main(String[] args) {
        int[] arr = {5, 3, 1, 4, 2};
        bubbleSort(arr);
        System.out.println(Arrays.toString(arr));
    }

    public static void bubbleSort(int[] arr) {
        for (int m = 1; m <= arr.length - 1; m++) {

            for (int n = 0; n < arr.length - m; n++) {
                int a = arr[n];
                int b = arr[n + 1];

                if (a > b) {
                    arr[n + 1] = a;
                    arr[n] = b;
                }
            }
        }
    }


}

