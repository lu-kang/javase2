package com.itheima.d3_lambda;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.IntFunction;

/**
 * 目标：使用Arrays的自定义排序方法
 */
public class Demo1 {
    public static void main(String[] args) {
        /*
            public static void sort(T[] arr, Comparator comparator) 对数组进行自定义排序
            public static void setAll(T[] array, IntFunction generator) 修改数组中的每一个元素
         */

        //需求1：对students数组中的元素按照年龄升序排列
        Student[] arr = {
                new Student("蜘蛛精", 169.5, 23),
                new Student("紫霞", 163.8, 23),
                new Student("牛魔王", 170.8, 28),
                new Student("至尊宝", 167.5, 24)
        };

        Arrays.sort(arr, new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return Integer.compare(o1.getAge(), o2.getAge());
            }
        });
        System.out.println(Arrays.toString(arr));


        //需求2：对数组中的每一个元素乘以0.8
        Double[] prices = {99.8, 128.0, 100.0};
        Arrays.setAll(prices, new IntFunction<Double>() {
            @Override
            public Double apply(int value) {
                return prices[value] * 0.8;
            }
        });
        System.out.println(Arrays.toString(prices));

    }
}
