package com.itheima.d3_lambda;

import java.util.Arrays;

/**
 * 目标：Lambda表达式快速入门
 */
public class Demo2 {
    public static void main(String[] args) {
        //需求1：对students数组中的元素按照年龄升序排列

        Student[] arr = {
                new Student("蜘蛛精", 169.5, 23),
                new Student("紫霞", 163.8, 23),
                new Student("牛魔王", 170.8, 28),
                new Student("至尊宝", 167.5, 24)
        };

        Arrays.sort(arr, (o1, o2) -> Integer.compare(o1.getAge(), o2.getAge()));

        System.out.println(Arrays.toString(arr));


        //需求2：对数组中的每一个元素乘以0.8
        Double[] prices = {99.8, 128.0, 100.0};
        Arrays.setAll(prices, value -> prices[value] * 0.8);
        System.out.println(Arrays.toString(prices));


    }
}
