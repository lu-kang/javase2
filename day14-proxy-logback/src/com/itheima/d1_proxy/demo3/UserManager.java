package com.itheima.d1_proxy.demo3;

//用户管理，实现具体的登录和注册操作
public class UserManager implements UserInterface {

    @Override
    public void login() {
        System.out.println("登录成功");
    }

    @Override
    public void register() {
        System.out.println("注册成功");
    }
}
