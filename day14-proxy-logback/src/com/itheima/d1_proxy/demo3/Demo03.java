package com.itheima.d1_proxy.demo3;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 需求：现有同事已经开发好了用户登录和注册功能：
 * <p>
 * 已有UserInterface接口，声明了login()登录、register()注册方法。
 * 已有UserManager实现了UserInterface接口，重写了login()和register()方法，实现了登录和注册功能。其中：login输出”登录成功”。 register输出”注册成功”。
 * 要求利用动态代理技术，在不改变同事代码的情况下，统计login()方法和register()方法的执行耗时。
 */
public class Demo03 {
    public static void main(String[] args) {
        //1 创建UserManager目标对象
        UserManager userManager = new UserManager();
        //2 使用Proxy.newProxyInstance()为目标对象创建代理对象
        ClassLoader loader = userManager.getClass().getClassLoader(); //作用：生成代理对象的class对象

        Class<?>[] interfaces = {UserInterface.class};//作用：告诉代理对象要和目标对象实现相同的接口，就具有相同名称的方法
        UserInterface proxyObj = (UserInterface) Proxy.newProxyInstance(loader, interfaces, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                //需求：针对目标对象中的所有方法添加耗时的功能
                //3 在InvocationHandler的invoke方法中定义代理规则
                //3.1 记录开始时间
                long start = System.nanoTime();  //毫秒 > 微秒 > 纳秒
                //3.2 反射调用目标对象方法
                Object result = method.invoke(userManager, args);
                //3.3 记录结束时间，两个时间做差就是耗时
                long end = System.nanoTime();
                System.out.println("共耗时：" + (end - start));
                return result;
            }
        });

        //4 调用代理对象的login()和register()方法
        proxyObj.login();
        proxyObj.register();
    }
}
