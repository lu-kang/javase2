package com.itheima.d1_proxy.demo3;

//用户接口
public interface UserInterface {

    //登录功能
    void login();

    //注册功能
    void register();
}
