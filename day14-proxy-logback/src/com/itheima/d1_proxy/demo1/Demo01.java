package com.itheima.d1_proxy.demo1;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 目标：演示JDK动态代理的实现方式
 * <p>
 * 需求：增强BigStar中的所有方法，在方法运行之前，运行之后多做一些事情
 */
public class Demo01 {
    public static void main(String[] args) {
        //1. 创建目标对象
        Star star = new BigStar();

        //2. 创建一个代理对象返回
        Star proxy = (Star) Proxy.newProxyInstance(
                star.getClass().getClassLoader(),
                star.getClass().getInterfaces(),
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        System.out.println("收首付款");
                        Object result = method.invoke(star, args);//反射执行被代理对象的方法
                        System.out.println("收尾款");
                        return result;
                    }
                });

        //3. 调用代理对象的sing和dance方法，观察输出结果。
        proxy.sing("我的中国心");
        System.out.println("-----------------");
        proxy.dance();
    }
}
