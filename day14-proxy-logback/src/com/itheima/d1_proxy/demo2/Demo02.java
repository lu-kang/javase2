package com.itheima.d1_proxy.demo2;


/**
 * 目标：掌握JDK动态代理的实现方式
 * <p>
 * 需求：增强BigStar中的所有方法，在方法运行之前，运行之后多做一些事情
 */
public class Demo02 {
    public static void main(String[] args) throws InterruptedException {
        //1. 创建目标对象
        //2. 创建一个代理对象返回
        //3. 调用代理对象的sing和dance方法，观察输出结果。
    }
}
