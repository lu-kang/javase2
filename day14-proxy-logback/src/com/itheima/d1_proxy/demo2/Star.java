package com.itheima.d1_proxy.demo2;

public interface Star {
    /**
     * 提供唱歌方法
     */
    String sing(String name);

    /**
     * 提供跳舞方法
     */
    String dance();
}
