package com.itheima.d2_log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 需求：利用logback日志技术，将日志信息打印在控制台
 * <p>
 * Logback日志技术导入步骤：
 * 1. 将资料中lib目录下的logback相关jar包复制到模块中，并add as library 添加为模块依赖。
 * 2. 创建Logback框架提供的Logger对象，然后用Logger对象调用其提供的方法就可以记录运行的日志信息
 */
public class Demo01 {

    public static void main(String[] args) {
        //1 创建Logger对象
        Logger log = LoggerFactory.getLogger(Demo01.class);  //输出哪个类中的日志就传哪个类的全类名或者Class对象
        //2 调用logger对象的info()方法输出日志信息
        for (int i = 0; i < 10000; i++) {
            log.info("main方法开始执行");
            log.info("天王盖地虎，小鸡炖蘑菇");
            log.info("main方法执行结束");
        }
    }
}
