package com.itheima.d2_log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 目标：掌握在程序中打印不同级别的日志
 * <p>
 * 级别程度排序：TRACE< DEBUG< INFO< WARN< ERROR
 * 使用方式：在root标签中的level属性中配置。默认级别是debug（忽略大小写）
 */
public class Demo02 {

    public static void main(String[] args) {
        Logger log = LoggerFactory.getLogger(Demo02.class);

        log.trace("trace级别日志信息...");
        log.debug("debug级别日志信息...");
        log.info("info级别日志信息...");
        log.warn("warn级别日志信息...");
        log.error("error级别日志信息...");
    }
}
