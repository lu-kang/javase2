package com.itheima.d2_char_io;

import java.io.FileReader;
import java.io.FileWriter;

/**
 * 目标：掌握FileReader字符输入流 读数据
 */
public class Demo01 {
    public static void main(String[] args) throws Exception {
        /*
            构造器：
                FileReader(File file)：创建字符输入流管道与源文件对象接通
                FileReader(String pathname)：创建字符输入流管道与源文件路径接通

            读取字符的方法：
                int read()：每次读取一个字符返回，如果字符已经没有可读的返回-1
                int read(char[] buffer)：每次读取一个字符数组，返回读取的字符个数，如果字符已经没有可读的返回-1

            需求：从a.txt文件中，使用文件字符输入流读取数据！一次读取一个字符
         */

        //1.创建字符输入流对象
        FileReader fr = new FileReader("d:/a.txt");
        //2.循环读取数据，一次读取一个字符
        int ch = 0;
        while ((ch = fr.read()) != -1) {
            System.out.print((char) ch);
        }
        //3 释放资源
        fr.close();
    }
}
