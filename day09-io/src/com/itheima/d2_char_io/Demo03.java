package com.itheima.d2_char_io;

import java.io.FileWriter;

/**
 * 目标：掌握FileWriter字符输出流 写数据
 */
public class Demo03 {

    public static void main(String[] args) throws Exception {
        /*
            构造器：
                 FileWriter(File file)：创建字符输出流管道与源文件对象接通
                 FileWriter(File file, boolean append)：创建字符输出流管道与源文件对象接通，可追加数据
                 FileWriter(String filepath)：创建字符输出流管道与源文件路径接通
                 FileWriter(String filepath, boolean append)：创建字符输出流管道与源文件路径接通，可追加数据

            写出数据的api：
                 write(int c)：写一个字符
                 write(char[] chars)：写入一个字符数组
                 write(char[] chars, int off, int len)：写入字符数组的一部分
                 write(String str)：写一个字符串
                 write(String str, int off, int len)：写一个字符串的一部分

            流关闭和刷新：
                 flush()：刷新流，还可以继续写数据
                 close()：关闭流，释放资源，但是在关闭之前会先刷新流。一旦关闭，就不能再写数据

                 注意：写出字符串"\r\n"也可以换行
         */

        //1.创建字符输出流对象，这里使用FileWriter。允许追加写出
        FileWriter fw = new FileWriter("d:/a.txt", true);
        //2.分别使用字符输出流的各个不同写出方法，写出数据
        fw.write(97);
        fw.write("\r\n");

        char[] chars = {'a', 'b', 'c'};
        fw.write(chars);
        fw.write("\r\n");

        fw.write("键盘敲烂，月薪过万\n");
        fw.write("键盘敲烂月薪过万", 0, 4);
        fw.write("\r\n");
        //3.释放资源
        fw.close();
    }
}
