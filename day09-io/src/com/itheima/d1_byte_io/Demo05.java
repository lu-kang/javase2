package com.itheima.d1_byte_io;

import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * 需求：把某个视频复制到其他目录下
 */
public class Demo05 {
    public static void main(String[] args) throws Exception {
        //1.创建字节输入、输出流对象
        FileInputStream fis = new FileInputStream("D:/1.avi");
        FileOutputStream fos = new FileOutputStream("D:/2.avi");

        //2.定义字节数组，接收每次读取的字节数据，放入数组中
        byte[] bys = new byte[1024];
        int len;//文件的字节总数不一定是1024的整数倍，所以最后一次很有可能装不满，定义一个变量，接收每次读取的字节个数
        //3.循环读取
        while ((len = fis.read(bys)) != -1) {
            //4.每次read方法执行结束，字节数组中就是新读取的字节数据
            //写入的时候，写入的字节数量就是得到的字节个数
            //防止最后一次的时候，如果没有读取1024个，会把默认的、不属于文件的0这个数据写进去
            fos.write(bys, 0, len);
        }

        //5.释放资源
        fis.close();
        fos.close();
    }
}
