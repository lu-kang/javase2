package com.itheima.d1_byte_io;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * 目标：掌握FileInputStream字节输入流 读数据
 */
public class Demo03 {
    public static void main(String[] args) throws IOException {
        /*
            构造器：
                FileInputStream(File file)：创建字节输入流管道与源文件对象接通
                FileInputStream(String pathname)：创建字节输入流管道与源文件路径接通

            相关方法：
                int read()：每次读取一个字节返回，如果字节已经没有可读的返回-1
                int read(byte[] buffer)：每次读取一个字节数组返回，如果字节已经没有可读的返回-1
                byte[] readAllBytes()：直接将当前字节输入流对应的文件对象的字节数据装到一个字节数组返回

            需求1：在d盘根目录创建一个a.txt文件，文件中有内容：java学科真牛批。请使用字节输入流读取数据，打印到控制台
            需求2：使用文件字节输入流一次读取一个字节数组，读取a.txt文件中的数据
            需求3：使用文件字节输入流一次读完全部字节，读取a.txt文件中的数据
         */

        //1.创建输入流对象
        FileInputStream fis = new FileInputStream("d:\\a.txt");
        //2.读取内容，使用readAllBytes()
        byte[] bytes = fis.readAllBytes();
        System.out.println(new String(bytes));
        //3.释放资源
        fis.close();
    }
}
