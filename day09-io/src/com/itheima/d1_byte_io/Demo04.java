package com.itheima.d1_byte_io;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 目标：掌握FileOutputStream字节输出流 写数据
 */
public class Demo04 {
    public static void main(String[] args) throws IOException {
        /*
            构造器：
                 FileOutputStream(File f)：创建字节输出流管道与源文件对象接通
                 FileOutputStream(File f, boolean append)：创建字节输出流管道与源文件对象接通，可追加数据
                 FileOutputStream(String filepath)：创建字节输出流管道与源文件路径接通
                 FileOutputStream(String filepath, boolean append)：创建字节输出流管道与源文件路径接通，可追加数据

            注意：创建FileOutputStream对象
                 如果文件不存在，会帮我们创建
                 如果文件存在，会把文件清空，添加append=true则不清空

            相关方法：
                 write(int a)：写一个字节出去
                 write(byte[] buffer)：写一个字节数组出去
                 write(byte[] buffer, int pos, int len)：写一个字节数组的一部分出去

                 在window中\r\n表示换行
         */
        FileOutputStream fos = new FileOutputStream("d:/b.txt", true);

        fos.write(97);
        fos.write("abcdefg".getBytes());
        fos.write("ABCDEFG".getBytes(), 1, 3);
        fos.write("\r\n".getBytes());
        //释放资源
        fos.close();
    }
}
