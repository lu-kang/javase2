package com.itheima.d3_buffer_io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.TreeSet;

/**
 * 目标：掌握字符缓冲流
 */
public class Demo02 {
    public static void main(String[] args) throws Exception {

        /*
            构造器：
                BufferedReader(Reader r)：把原始的字符输入流包装成高效的字符缓冲输入流管道，从而提高字符输入流读数据的性能
                BufferedWriter(Writer w)：把原始的字符输出流包装成高级的缓冲字符输出流管道，从而提高字符输出流写数据的性能

            需求：拷贝出师表（资料中csb.txt）到另一个文件，并恢复顺序
         */

        //1.创建字符缓冲输入流和字符缓冲输出流对象。
        BufferedReader br = new BufferedReader(new FileReader("d:/csb.txt"));
        BufferedWriter bw = new BufferedWriter(new FileWriter("d:/csb2.txt"));

        //2.定义一个TreeSet集合存储读取的每行数据。
        TreeSet<String> treeSet = new TreeSet<>();
        //3.循环读取每一行数据，存入到TreeSet集合中去。
        String line;
        while ((line = br.readLine()) != null) {
            treeSet.add(line);
        }
        //4.遍历TreeSet集合,将每个元素通过字符缓冲输出流追加到新文件中，并换行。
        for (String s : treeSet) {
            bw.write(s);  //写出数据
            bw.newLine(); //换行
        }
        //5.释放资源。
        br.close();
        bw.close();
    }
}
