package com.itheima.d3_buffer_io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * 目标：掌握字节缓冲流
 */
public class Demo01 {
    public static void main(String[] args) throws Exception {

         /*
            构造器：
                BufferedInputStream(InputStream is)：把原始的字节输入流包装成一个高效的缓冲字节输入流，从而提高读数据的性能
                BufferedOutputStream(OutputStream os)：把原始的字节输出流包装成一个高效的缓冲字节输出流，从而提高写数据的性能

            需求：把某个视频复制到其他目录下
         */

        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("d:/1.avi"));
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("d:/2.avi"));

        //2.一次读取一个字节数据
        byte[] bytes = new byte[1024];
        int len;
        while ((len = bis.read(bytes)) != -1) {
            bos.write(bytes, 0, len);
        }

        //3.释放资源
        bis.close();
        bos.close();
    }
}
