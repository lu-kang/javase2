package com.itheima.d3_buffer_io;

import java.io.*;

/**
 * 需求：定义方法分别使用原始的字节流，以及字节缓冲流复制一个文件(10MB就行)，记录耗时。
 */
public class Demo03 {
    public static void main(String[] args) throws IOException {
        /*
            方案1. 原始字节流按一个字节形式复制文件
            方案2. 原始字节流按字节数组形式复制文件
            方案3. 缓冲字节流按一个字节形式复制文件
            方案4. 高效缓冲字节流按字节数组形式复制文件
         */
        copy4();
        copy3();
        copy2();
        copy1();
    }

    //1. 原始字节流按一个字节形式复制文件
    public static void copy1() throws IOException {
        //获取开始时间，毫秒值
        long start = System.currentTimeMillis();

        FileInputStream fis = new FileInputStream("d:/test.avi");
        FileOutputStream fos = new FileOutputStream("d:/test2.avi");
        int by;
        while ((by = fis.read()) != -1) {
            fos.write(by);
        }
        fis.close();
        fos.close();

        //获取结束时间，毫秒值
        long end = System.currentTimeMillis();
        System.out.println("1. 原始字节流按一个字节形式复制文件，耗时：" + (end - start));
    }


    //2. 原始字节流按字节数组形式复制文件
    public static void copy2() throws IOException {
        //获取开始时间，毫秒值
        long start = System.currentTimeMillis();

        FileInputStream fis = new FileInputStream("d:/test.avi");
        FileOutputStream fos = new FileOutputStream("d:/test2.avi");
        int len;
        byte[] bytes = new byte[1024];
        while ((len = fis.read(bytes)) != -1) {
            fos.write(bytes, 0, len);
        }
        fis.close();
        fos.close();

        //获取结束时间，毫秒值
        long end = System.currentTimeMillis();
        System.out.println("2. 原始字节流按字节数组形式复制文件，耗时：" + (end - start));
    }


    //3. 缓冲字节流按一个字节形式复制文件
    public static void copy3() throws IOException {
        //获取开始时间，毫秒值
        long start = System.currentTimeMillis();

        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("d:/test.avi"));
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("d:/test2.avi"));
        int by;
        while ((by = bis.read()) != -1) {
            bos.write(by);
        }
        bis.close();
        bos.close();

        //获取结束时间，毫秒值
        long end = System.currentTimeMillis();
        System.out.println("3. 缓冲字节流按一个字节形式复制文件，耗时：" + (end - start));
    }


    //4. 高效缓冲字节流按字节数组形式复制文件
    public static void copy4() throws IOException {
        //获取开始时间，毫秒值
        long start = System.currentTimeMillis();

        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("d:/test.avi"));
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("d:/test2.avi"));
        int len;
        byte[] bytes = new byte[1024];
        while ((len = bis.read(bytes)) != -1) {
            bos.write(bytes, 0, len);
        }
        bis.close();
        bos.close();

        //获取结束时间，毫秒值
        long end = System.currentTimeMillis();
        System.out.println("4. 高效缓冲字节流按字节数组形式复制文件，耗时：" + (end - start));
    }
}
