package com.itheima.d5_release_io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.TreeSet;

/**
 * 目标：掌握try-with-resource资源释放
 */
public class Demo02 {
    public static void main(String[] args) throws Exception {
        /*
            基本做法：在finally代码块中执行所有资源释放操作
            JDK7改进方案：在try后加上小括号，在其内定义流对象，使用分号隔开，即可自动释放
         */
        try (BufferedReader br = new BufferedReader(new FileReader("d:/csb.txt"));
             BufferedWriter bw = new BufferedWriter(new FileWriter("d:/csb2.txt"))) {
            //1. 创建字符缓冲输入流和字符缓冲输出流对象。

            //2. 定义一个treeSet集合存储读取的每行数据。
            TreeSet<String> treeSet = null;

            //3. 循环读取每一行数据，存入到treeSet集合中去。
            String line;
            while ((line = br.readLine()) != null) {
                treeSet.add(line);
            }

            //4. 遍历treeSet集合,将每个元素通过字符缓冲输出流追加到新文件中，并换行。
            for (String s : treeSet) {
                bw.write(s);  //写出数据
                bw.newLine(); //换行
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //5. 资源释放
    }
}
