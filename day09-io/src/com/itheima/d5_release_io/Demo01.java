package com.itheima.d5_release_io;

import java.io.*;
import java.util.TreeSet;

/**
 * 目标：掌握try-catch-finally资源释放
 */
public class Demo01 {
    public static void main(String[] args) throws IOException {
        /*
            基本做法：在finally代码块中执行所有资源释放操作
         */

        BufferedReader br = null;
        BufferedWriter bw = null;
        try {
            //1. 创建字符缓冲输入流和字符缓冲输出流对象。
            br = new BufferedReader(new FileReader("d:/csb.txt"));
            bw = new BufferedWriter(new FileWriter("d:/csb2.txt"));

            //2. 定义一个treeSet集合存储读取的每行数据。
            TreeSet<String> treeSet = null;

            //3. 循环读取每一行数据，存入到treeSet集合中去。
            String line;
            while ((line = br.readLine()) != null) {
                treeSet.add(line);
            }

            //4. 遍历treeSet集合,将每个元素通过字符缓冲输出流追加到新文件中，并换行。
            for (String s : treeSet) {
                bw.write(s);  //写出数据
                bw.newLine(); //换行
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //5. 资源释放
            if (br != null) {
                br.close();
            }
            if (bw != null) {
                bw.close();
            }
        }
    }
}
