package com.itheima.d4_transform_io;

import java.io.*;

/**
 * 问题：不同编码读取、写入时会乱码
 */
public class Demo01 {
    public static void main(String[] args) throws IOException {

        BufferedReader br1 = new BufferedReader(new FileReader("d:/gbk.txt"));
        BufferedReader br2 = new BufferedReader(new FileReader("d:/utf8.txt"));

        System.out.println(br1.readLine());
        System.out.println(br2.readLine());

        br1.close();
        br2.close();

        //===========================================================================================

        BufferedWriter bw1 = new BufferedWriter(new FileWriter("d:/gbk.txt", true));
        BufferedWriter bw2 = new BufferedWriter(new FileWriter("d:/utf8.txt", true));

        bw1.write("黑马程序员\r\n");
        bw2.write("黑马程序员\r\n");

        bw1.close();
        bw2.close();
    }
}
