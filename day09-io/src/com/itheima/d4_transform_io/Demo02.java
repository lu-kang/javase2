package com.itheima.d4_transform_io;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * 目标：掌握InputStreamReader字符输入转换流
 */
public class Demo02 {
    public static void main(String[] args) throws Exception {
        /*
            构造器：
                InputStreamReader(InputStream is)：几乎不用，可以把原始的字节流按照代码默认编码转换成字符输入流
                InputStreamReader(InputStream is, String charset)：可以把原始的字节流按照指定编码转换成字符输入流，这样字符流中的字符就不乱码了(重点)

            需求：使用字符输入转换流读取gbk.txt文件内容，使其读取内容不乱码
         */

        BufferedReader br1 =
                new BufferedReader(
                        new InputStreamReader(
                                new FileInputStream("d:/gbk.txt"), "GBK"));

        BufferedReader br2 = new BufferedReader(new FileReader("d:/gbk.txt", Charset.forName("GBK")));

        System.out.println(br1.readLine());
        System.out.println(br2.readLine());

        br1.close();
        br2.close();
    }
}
