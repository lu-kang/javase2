package com.itheima.d4_transform_io;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

/**
 * 目标：掌握OutputStreamWriter字符输出转换流
 */
public class Demo03 {
    public static void main(String[] args) throws Exception {

        /*
            构造器：
                OutputStreamWriter(OutputStream os)：作用和FileWriter一样，可以把原始的字节输出流按照代码默认编码转换成字符输出流。几乎不用
                OutputStreamWriter(OutputStream os, String charset)：可以把原始的字节输出流按照指定编码转换成字符输出流

            需求：将字符串黑马程序员以GBK编码方式写出到gbk.txt文件中
         */

        BufferedWriter bw1 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("d:/gbk.txt", true), "GBK"));
        BufferedWriter bw2 = new BufferedWriter(new FileWriter("d:/gbk.txt", Charset.forName("GBK"), true));

        bw1.write("黑马程序员\r\n");
        bw2.write("黑马程序员\r\n");

        bw1.close();
        bw2.close();
    }
}
