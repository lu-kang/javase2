package com.itheima.d2_set;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 * 目标：TreeSet集合自定义排序规则
 * <p>
 * 方式一：让元素类实现Comparable接口，实现compareTo方法
 * 方式二：在创建TreeSet集合时，传递一个比较器对象，比较器是Comparator接口的实现类对象
 */
public class Demo4 {
    public static void main(String[] args) {
        //1.创建集合对象
        Set<Student> students = new TreeSet<>(new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return Double.compare(o1.getAge(), o2.getAge());
            }
        });
        //2.创建学生对象添加到集合中
        Student s1 = new Student("至尊宝", 20);
        Student s2 = new Student("蜘蛛精", 23);
        Student s3 = new Student("蜘蛛精", 23);
        Student s4 = new Student("牛魔王", 48);
        //3.遍历打印集合中的学生
        students.add(s1);
        students.add(s2);
        students.add(s3);
        students.add(s4);

        for (Student s : students) {
            System.out.println(s);
        }
    }
}
