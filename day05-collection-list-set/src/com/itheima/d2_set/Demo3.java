package com.itheima.d2_set;

import java.util.HashSet;
import java.util.Set;

/**
 * 案例：Set集合存储学生对象并去重
 * <p>
 * 要求：如果学生对象的所有成员变量的值相同，我们就认为是同一个对象，需要去重
 */
public class Demo3 {
    public static void main(String[] args) {
        //1.创建集合对象
        Set<Student> students = new HashSet<>();
        //2.创建学生对象添加到集合中
        Student s1 = new Student("至尊宝", 20);
        Student s2 = new Student("蜘蛛精", 23);
        Student s3 = new Student("蜘蛛精", 23);
        Student s4 = new Student("牛魔王", 48);
        //3.遍历打印集合中的学生
        students.add(s1);
        students.add(s2);
        students.add(s3);
        students.add(s4);

        for (Student s : students) {
            System.out.println(s);
        }
    }
}
