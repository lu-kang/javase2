package com.itheima.d2_set;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * 目标：演示Set集合的特点（运行下面的代码总结HashSet集合、LinkedHashSet集合、TreeSet集合的元素特点）
 */
public class Demo1 {
    public static void main(String[] args) {
        HashSet<String> set1 = new HashSet<>(); //HashSet无序、不重复、无索引。
        set1.add("i");
        set1.add("t");
        set1.add("h");
        set1.add("m");
        set1.add("m");
        set1.add("a");
        System.out.println("HashSet集合：" + set1);

        System.out.println("-------------------------------------");

        LinkedHashSet<String> set2 = new LinkedHashSet<>(); //LinkedHashSet 有序、不重复、无索引。
        set2.add("i");
        set2.add("t");
        set2.add("h");
        set2.add("m");
        set2.add("m");
        set2.add("a");
        System.out.println("LinkedHashSet集合：" + set2);

        System.out.println("---------------------------------------");

        TreeSet<String> set3 = new TreeSet<>(); //TreeSet 可排序、不重复、无索引
        set3.add("c");
        set3.add("b");
        set3.add("d");
        set3.add("a");
        set3.add("a");
        System.out.println("TreeSet集合：" + set3);
    }
}
