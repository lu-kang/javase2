package com.itheima.d2_set;

/**
 * 目标：演示hashCode()方法的作用
 */
public class Demo2 {
    public static void main(String[] args) {
        //需求：创建2个Student对象，并获取这两个Student对象的哈希值，观察哈希值是否相同
        Student s1 = new Student("张三", 18);
        Student s2 = new Student("张三", 18);

        System.out.println(s1.hashCode());
        System.out.println(s1.hashCode());
        System.out.println(s2.hashCode());
        System.out.println(s2.hashCode());

        System.out.println(s1.getName().hashCode());
        System.out.println(s2.getName().hashCode());

        System.out.println("Aa".hashCode());
        System.out.println("BB".hashCode());
    }
}
