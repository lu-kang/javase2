package com.itheima.d2_list;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * 目标：LinkedList的应用场景、做队列和栈使用
 * <p>
 * public void addFirst(E e) 在该列表开头插入指定的元素
 * public void addLast(E e) 将指定的元素追加到此列表的末尾
 * public E removeFirst() 从此列表中删除并返回第一个元素
 * public E removeLast() 从此列表中删除并返回最后一个元素
 */
public class Demo4 {
    public static void main(String[] args) {
        //需求1：使用LinkedList集合模拟队列结构进队列、出队列

        //1.创建一个队列：先进先出
        LinkedList<String> queue = new LinkedList<>();
        //入对列
        queue.addFirst("第1号人");
        queue.addFirst("第2号人");
        queue.addFirst("第3号人");
        System.out.println(queue);
        //出队列
        System.out.println(queue.removeLast());    //第1号人
        System.out.println(queue.removeLast());    //第2号人
        System.out.println(queue.removeLast());    //第3号人


        //需求2：使用LinkedList集合模拟栈结构压栈、弹栈
        //1.创建一个栈对象：先进后出
        LinkedList<String> stack = new LinkedList<>();
        //压栈(push) 等价于 addFirst()
        stack.push("第1颗子弹");
        stack.push("第2颗子弹");
        stack.push("第3颗子弹");
        System.out.println(stack);

        //弹栈(pop) 等价于 removeFirst()
        System.out.println(stack.pop()); //第3颗子弹
        System.out.println(stack.pop()); //第2颗子弹
        System.out.println(stack.pop()); //第1颗子弹

        //弹栈完了，集合中就没有元素了
        System.out.println(stack);
    }
}
