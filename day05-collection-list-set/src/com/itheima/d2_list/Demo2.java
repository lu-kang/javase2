package com.itheima.d2_list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 目标：掌握List集合的遍历方式（4种）
 * <p>
 * 1.for遍历
 * 2.迭代器遍历
 * 3.增强for遍历
 * 4.forEach()遍历
 */
public class Demo2 {
    public static void main(String[] args) {

        //1.创建集合对象，保存数据
        List<String> list = new ArrayList<>();
        list.add("蜘蛛精");
        list.add("至尊宝");
        list.add("糖宝宝");

        //1.普通for循环
        for (int i = 0; i < list.size(); i++) {
            String e = list.get(i);
            System.out.println(e);
        }

        System.out.println("===============");

        //2.增强for遍历
        for (String s : list) {
            System.out.println(s);
        }

        System.out.println("===============");

        //3.迭代器遍历
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            String s = it.next();
            System.out.println(s);
        }

        System.out.println("===============");

        //4.forEach遍历
        list.forEach(s -> System.out.println(s));

    }
}
