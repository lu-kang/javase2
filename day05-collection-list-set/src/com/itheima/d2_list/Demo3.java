package com.itheima.d2_list;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * 目标：ArrayList集合、LinkedList集合底层原理
 * <p>
 * ArrayList集合底层原理：
 * (1)当创建一个ArrayList集合，并添加第一个元素时，底层会创建一个长度为10的数组
 * (2)往集合中添加元素时，实际上是往底层数组中添加元素
 * (3)当数组中的初始10个元素存满了，会创建一个新数组是原来数组的1.5倍，将原数组中的元素复制到新数组中去，并将新数组作为ArrayList的容器
 * (4)再添加元素时，就往新数组中添加，直到把数组存满，再重复前面的步骤对数组进行扩容
 * 特点：由于ArrayList集合是数组实现，每次添加、删除元素时会涉及到数组的扩容，和元素的复制增删速度相对较慢
 * <p>
 * LinkedList集合底层原理：
 * (1)LinkedList集合是双向链表结构实现的，每一个元素就是链表中的一个节点，
 * (2)每一个节点包含三个部分，元素值、上一个节点地址、下一个节点地址
 * (3)当往LinkedList集合中添加元素时，实际上是往链表的尾结点位置添加一个新节点
 * 特点：由于LinkedList底层是链表结构，每次查询元素时要么从头往尾查，要么从尾往头查，所以查询较慢；增、删元素时，只需要重新设定节点地址就行，所以相对较快
 */
public class Demo3 {

    public static void main(String[] args) {
        //1.创建ArrayList集合，添加数据
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("脉劫");

        //2.创建LinkedList集合，添加数据
        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.add("脉劫");

    }
}
