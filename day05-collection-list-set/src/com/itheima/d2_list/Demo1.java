package com.itheima.d2_list;

import java.util.ArrayList;
import java.util.List;

/**
 * 目标：学习List集合特有方法
 */
public class Demo1 {
    public static void main(String[] args) {

        //1.创建一个ArrayList集合对象（有序、有索引、可以重复）
        List<String> list = new ArrayList<>();
        list.add("蜘蛛精");
        list.add("至尊宝");
        list.add("至尊宝");
        list.add("牛夫人");
        System.out.println(list);

        //2.public void add(int index, E element): 在某个索引位置插入元素
        list.add(2, "紫霞仙子");
        System.out.println(list);

        //3.public E remove(int index): 根据索引删除元素, 返回被删除的元素
        System.out.println(list.remove(2)); //紫霞仙子
        System.out.println(list);

        //4.public E get(int index): 返回集合中指定位置的元素
        System.out.println(list.get(3));

        //5.public E set(int index, E e): 修改索引位置处的元素，修改后，会返回原数据
        System.out.println(list.set(3, "牛魔王"));
        System.out.println(list);

    }
}
