package com.itheima.d1_collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.function.Consumer;

/**
 * 目标：学习Collection集合的遍历方式（3种）
 * <p>
 * 1. 迭代器遍历
 * 2. 增强for遍历
 * 3. forEach遍历
 */
public class Demo3 {
    public static void main(String[] args) {
        Collection<String> collection = new ArrayList<>();
        collection.add("脉劫");
        collection.add("康帅傅");
        collection.add("粤利粤");
        collection.add("大个核桃");

        //需求：使用迭代器遍历集合中的每一个元素，并打印输出
        Iterator<String> iterator = collection.iterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            System.out.println(next);
        }

        System.out.println("=========================");

        //需求：使用增强for遍历集合中的每一个元素，并打印输出
        for (String s : collection) {
            System.out.println(s);
        }

        System.out.println("=========================");

        //需求：使用forEach方法遍历集合中的每一个元素，并打印输出
        collection.forEach(System.out::println);
    }
}
