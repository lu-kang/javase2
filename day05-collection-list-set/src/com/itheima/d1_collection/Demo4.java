package com.itheima.d1_collection;

import java.util.ArrayList;
import java.util.List;

/**
 * 案例：每一部电影有名称name、评分score、主演actor。展示所有电影信息。
 * <p>
 * 《肖申克的救赎》, 9.7, 罗宾斯
 * 《霸王别姬》, 9.6, 张国荣、张丰毅
 * 《阿甘正传》, 9.5, 汤姆.汉斯克
 */
public class Demo4 {
    public static void main(String[] args) {
        //1.创建集合对象
        List<Movie> list = new ArrayList<>();
        //2.向集合中添加电影对象
        list.add(new Movie("《肖申克的救赎》", 9.7, "罗宾斯"));
        list.add(new Movie("《霸王别姬》", 9.6, "张国荣、张丰毅"));
        list.add(new Movie("《阿甘正传》", 9.5, "汤姆.汉斯克"));
        //3.遍历集合并打印每个对象
        list.forEach(m -> System.out.println(m));
    }
}