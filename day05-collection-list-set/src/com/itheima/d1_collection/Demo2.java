package com.itheima.d1_collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * 目标：学习Collection集合的常用方法
 */
public class Demo2 {
    public static void main(String[] args) {
        Collection<String> c = new ArrayList<>();
        //1.public boolean add(E e): 添加元素到集合
        c.add("java1");
        c.add("java1");
        c.add("java2");
        c.add("java2");
        c.add("java3");
        System.out.println(c);

        //2.public int size(): 获取集合的大小
        System.out.println(c.size());

        //3.public boolean contains(Object obj): 判断集合中是否包含某个元素
        System.out.println(c.contains("java1"));

        //4.public boolean remove(E e): 删除某个元素，如果有多个重复元素只能删除第一个
        System.out.println(c.remove("java1"));
        System.out.println(c);

        //5.public boolean isEmpty(): 判断集合是否为空 是空返回true 反之返回false
        System.out.println(c.isEmpty());

        //6.public Object[] toArray(): 把集合转换为数组
        Object[] array = c.toArray();
        System.out.println(Arrays.toString(array));

        //7.还可以把一个集合中的元素，添加到另一个集合中
        Collection<String> c1 = new ArrayList<>();
        c1.add("java1");
        c1.add("java2");
        c.addAll(c1); //把c1集合中的全部元素，添加到c集合中去
        System.out.println(c);

        //8.public void clear(): 清空集合的元素
        c.clear();
        System.out.println(c);
    }
}
