package com.itheima.d2_map;

import java.util.HashMap;
import java.util.Map;

/**
 * 案例：HashMap集合存储自定义对象并遍历。创建一个HashMap集合，键是学生对象(Student)，值是籍贯(String)。存储三个键值对元素，并遍历
 */
public class Demo4 {
    public static void main(String[] args) {

        Map<Student, String> map = new HashMap<>();

        map.put(new Student("蜘蛛精", 25), "盘丝洞");
        map.put(new Student("蜘蛛精", 25), "水帘洞");
        map.put(new Student("至尊宝", 23), "水帘洞");
        map.put(new Student("牛魔王", 28), "牛头山");

        map.forEach((student, s) -> {
            System.out.println(student + " -----> " + s);
        });
    }
}
