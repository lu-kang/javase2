package com.itheima.d2_map;

import java.util.Map;
import java.util.TreeMap;

/**
 * 目标：TreeMap自定义排序规则
 * 需求：创建一个TreeMap集合，键是学生对象(Student)，值是籍贯(String)。学生属性姓名和年龄，按照年龄进行排序并遍历。
 */
public class Demo5 {
    public static void main(String[] args) {


        /*Map<Student, String> map = new TreeMap<>(new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return Double.compare(o1.getAge(), o2.getAge());
            }
        });*/

        Map<Student, String> map = new TreeMap<>();

        map.put(new Student("蜘蛛精", 25), "盘丝洞");
        map.put(new Student("蜘蛛精", 25), "水帘洞");
        map.put(new Student("至尊宝", 23), "水帘洞");
        map.put(new Student("牛魔王", 28), "牛头山");


        map.entrySet().forEach(e -> System.out.println(e));
    }
}
