package com.itheima.d2_map;

import java.util.*;

/**
 * 案例：Map集合案例-省和市
 * <p>
 * 需求：在程序中记住如下省份和其对应的城市信息，记录成功后，要求可以查询出湖北省的城市信息。
 * 江苏省 = "南京市","扬州市","苏州市","无锡市","常州市"
 * 湖北省 = "武汉市","孝感市","十堰市","宜昌市","鄂州市"
 * 河北省 = "石家庄市","唐山市","邢台市","保定市","张家口市"
 */
public class Demo6 {
    public static void main(String[] args) {
        // 1、定义一个Map集合存储全部的省份信息，和其对应的城市信息。
        Map<String, List<String>> map = new HashMap<>();
        map.put("江苏省", List.of("南京市", "扬州市", "苏州市", "无锡市", "常州市"));
        map.put("湖北省", List.of("武汉市", "孝感市", "十堰市", "宜昌市", "鄂州市"));
        map.put("河北省", List.of("石家庄市", "唐山市", "邢台市", "保定市", "张家口市"));

        System.out.println(List.of("南京市", "扬州市", "苏州市", "无锡市", "常州市").getClass());

        List<String> list = map.get("湖北省");
        System.out.println(list);

        map.forEach((p, c) -> {
            System.out.println(p + " -----> " + c);
        });
    }
}
