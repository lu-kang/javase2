package com.itheima.d2_map;

import java.util.*;

/**
 * 案例：map集合的案例-统计投票人数。
 * 某个班级80名学生，现在需要组织秋游活动，班长提供了四个景点依次是（A、B、C、D）,每个学生只能选择一个景点，请统计出最终哪个景点想去的人数最多。
 */
public class Demo3 {
    public static void main(String[] args) {
        //1.定义数组保存80名学生的投票结果
        String[] votes = {"C", "B", "A", "D", "B", "B", "D", "C", "D", "C", "A", "C",
                "A", "B", "C", "D", "A", "B", "A", "B", "B", "D", "A", "B", "D", "D",
                "C", "C", "D", "C", "B", "A", "C", "B", "D", "C", "B", "D", "B", "B",
                "B", "A", "B", "C", "B", "A", "C", "C", "B", "A", "A", "B", "B", "D",
                "B", "C", "A", "A", "D", "A", "D", "A", "C", "B", "B", "B", "A", "A",
                "D", "D", "C", "C", "D", "B", "B", "B", "D", "A", "C", "A"};

        // 2.开始统计每个景点的投票人数
        // 准备一个Map集合用于统计最终的结果
        Map<String, Integer> result = new HashMap<>();

        // 3.开始遍历80个景点数据
        for (String s : votes) {
            // 问问Map集合中是否存在该景点
            if (result.containsKey(s)) {
                // 说明这个景点之前统计过。其值+1. 存入到Map集合中去
                result.put(s, result.get(s) + 1);
            } else {
                // 说明这个景点是第一次统计，存入"景点=1"
                result.put(s, 1);
            }
        }
        System.out.println(result);
    }
}
