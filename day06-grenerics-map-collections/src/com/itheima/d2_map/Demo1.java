package com.itheima.d2_map;

import java.util.HashMap;
import java.util.Map;

/**
 * 目标：演示Map集合的常用方法
 */
public class Demo1 {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();

        /*
            1. public V put(K key, V value) 添加元素，无序，不重复，无索引。当key不存在时添加，存在时修改
            2. public int size():获取集合的大小
            3. public boolean isEmpty() 判断集合是否为空，为空返回true
            4. public V get(Object obj) 根据键获取对应值
            5. public V remove(Object key) 根据键删除整个元素,返回被删除的值
            6. public boolean containsKey(Object key) 判断是否包含某个键
            7. public boolean containsValue(Object key) 判断是否包含某个值
            8. public Set<K> keySet() 获取全部键的集合
            9. public Collection<V> values() 获取全部值的集合
            10.public void clear() 清空集合
         */

        map.put("手表", 100);
        map.put("手表", 220);
        map.put("手机", 2);
        map.put("Java", 2);
        map.put(null, null);

        System.out.println(map);
        System.out.println(map.size());
        System.out.println(map.isEmpty());
        System.out.println(map.get("手表"));
        System.out.println(map.get("手机")); // 2
        System.out.println(map.get("张三")); // null
        System.out.println(map.remove("手表"));
        System.out.println(map);
        System.out.println(map.containsKey("手表")); // false
        System.out.println(map.containsKey("Java")); // true
        System.out.println(map.containsValue(2)); // true
        System.out.println(map.keySet());
        System.out.println(map.values());
        map.clear();
    }
}
