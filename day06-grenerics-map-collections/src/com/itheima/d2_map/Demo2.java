package com.itheima.d2_map;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

/**
 * 目标：演示Map集合的遍历（三种方式）
 */
public class Demo2 {
    public static void main(String[] args) {
        Map<String, Double> map = new HashMap<>();
        map.put("蜘蛛精", 162.5);
        map.put("紫霞", 165.8);
        map.put("至尊宝", 169.5);
        map.put("牛魔王", 183.6);

        //方式一：键找值
        //1.获取所有的键的集合 public Set<K> keySet()
        //2.遍历所有的键，根据键从map集合中获取对应的value值
        Set<String> keys = map.keySet();
        for (String key : keys) {
            double value = map.get(key);
            System.out.println(key + " = " + value);
        }

        System.out.println("-------------------------------");

        //方式二：键值对
        //1.获取键值对对象（Entry对象）集合
        //2.遍历集合，获取每一个键值对对象的key和value打印
        Set<Map.Entry<String, Double>> entries = map.entrySet();
        for (Map.Entry<String, Double> entry : entries) {
            String key = entry.getKey();
            double value = entry.getValue();
            System.out.println(key + " = " + value);
        }

        System.out.println("-------------------------------");

        //方式三：forEach()遍历
        map.forEach(new BiConsumer<String, Double>() {
            @Override
            public void accept(String k, Double v) {
                System.out.println(k + " = " + v);
            }
        });
    }
}
