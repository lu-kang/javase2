package com.itheima.d1_generic.demo5;

import java.util.ArrayList;
import java.util.List;

/**
 * 目标：演示泛型通配符、上下限、泛型擦除
 * <p>
 * http://javare.cn/
 */
public class Demo5 {

    public static void main(String[] args) {
        print1(new ArrayList<String>());
        print2(new ArrayList<Cat>());
        print3(new ArrayList<Animal>());
    }

    //泛型通配符：?表示任意类型
    public static void print1(List<?> list) {
        for (Object animal : list) {
            System.out.println(animal);
        }
    }

    //泛型的上限：?表示Animal子类
    public static void print2(List<? extends Animal> list) {
        for (Animal animal : list) {
            System.out.println(animal);
        }
    }

    //泛型的下限：?表示Cat父类
    public static void print3(List<? super Cat> list) {
        for (Object animal : list) {
            System.out.println(animal);
        }
    }
}
