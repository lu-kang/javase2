package com.itheima.d1_generic.demo3;

public interface A<T> {

    //在类的成员方法中都可以使用泛型T
    void show(T t);
}
