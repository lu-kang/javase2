package com.itheima.d1_generic.demo3;

public class B implements A<String> {

    //重写接口中的方法，如果方法中使用到了泛型，此时都会被String代替
    @Override
    public void show(String s) {
        System.out.println(s);
    }
}
