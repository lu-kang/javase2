package com.itheima.d1_generic.demo2;

/**
 * 目标：在类上声明泛型
 */
public class Demo2 {

    public static void main(String[] args) {
        Result<String> r1 = new Result<>("ok");

        Result<Integer> r2 = new Result<>(1);
    }
}
