package com.itheima.d1_generic.demo2;

public class Result<T> {

    private T data;

    public Result(T data) {
        this.data = data;
    }
}
