package com.itheima.d1_generic.demo4;

/**
 * 目标：在方法上声明泛型
 */
public class Demo4 {

    public static void main(String[] args) {
        m1("java");
        m1(100);
        m1(13.14);
    }

    public static <T> void m1(T t) {
        if (t instanceof String) System.out.println(t);
        if (t instanceof Integer) System.out.println((Integer) t + 1);
        if (t instanceof Double) System.out.println((Double) t + 0.01);
    }
}

