package com.itheima.d1_generic.demo1;

import java.util.ArrayList;
import java.util.List;

/**
 * 目标：认识泛型，演示泛型在ArrayList集合中的作用
 */
public class Demo1 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();

        list.add("java");
        list.add("python");
        list.add("php");
        list.add("c++");

        System.out.println(list);
    }
}
