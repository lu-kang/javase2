package com.itheima.d3_collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 目标：演示Collections工具类的常用方法
 */
public class Demo2 {
    public static void main(String[] args) {

        /*
            1. public static <T> boolean addAll(Collection c, T...elements) 给集合批量添加元素
            2. public static void shuffle(List<?> list) 打乱List集合中的元素顺序
            3. public static <T> void sort(List<T> list) 对List集合中的元素进行升序排序
            4. public static <T> void sort(List<T> list, Comparator c) 对List集合中元素按照比较器对象指定的规则进行排序
         */

        List<String> names = new ArrayList<>();
        Collections.addAll(names, "Bob", "Jum", "Jack", "Ana", "Coco");
        System.out.println(names);

        Collections.shuffle(names);
        System.out.println(names);

        Collections.sort(names);
        System.out.println(names);

        //自定义排序规则
        Collections.sort(names, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length() - o2.length();
            }
        });
        System.out.println(names);
    }
}
