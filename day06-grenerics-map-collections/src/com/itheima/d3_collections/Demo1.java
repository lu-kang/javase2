package com.itheima.d3_collections;

import java.util.Arrays;

/**
 * 目标：演示可变参数的用法
 */
public class Demo1 {

    public static void main(String[] args) {
        //需求：定义一个方法，可以求任意几个整数的
        System.out.println(test(1));
        System.out.println(test(1, 2));
        System.out.println(test(1, 2, 3));
    }

    public static int test(int... nums) {
        //可变参数在方法内部，本质上是一个数组
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        return sum;
    }

}
