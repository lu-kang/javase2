package com.itheima.d3_collections;

import java.util.*;

/**
 * 案例：斗地主。在启动游戏房间的时候，应该提前准备好54张牌，接着，需要完成洗牌、发牌、对牌排序、看牌。（斗地主：发出51张牌，剩下3张做为底牌。）
 * <p>
 * 分析：
 * 牌的表示形式： ♠3 ♥3 ♣3 ♦3、♠4 ♥4 ♣4 ♦4、♠5 ♥5 ♣5 ♦5、... ♠2 ♥2 ♣2 ♦2、🃟 🃏
 * 每张牌的大小： 1   2  3  4    5   6  7  8  ...                 51 52 53  54
 * 存储扑克牌的牌盒：Map<Integer,String> poker = new HashMap<>(); 键是牌的序号(大小)，值是牌
 */
public class Demo3 {
    public static void main(String[] args) {
        //1. 准备Map集合，用于存放54张牌
        Map<Integer, String> poker = new HashMap<>();
        List<String> numbers = List.of("3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A", "2");
        List<String> colors = List.of("♠", "♥", "♣", "♦");
        int size = 1;  //记录序号（大小），表示每张牌的大小
        for (String number : numbers) {
            for (String color : colors) {
                poker.put(size++, color + number);
            }
        }

        poker.put(size++, "🃟");
        poker.put(size, "🃏");

        //2. 洗牌，打乱poker集合中的所有键，后期根据键获取值。
        List<Integer> pokerKeys = new ArrayList<>(poker.keySet()); //poker.keySet()获取poker集合中所有的key
        Collections.shuffle(pokerKeys);

        //3. 发牌
        TreeMap<Integer, String> t0 = new TreeMap<>(); //存放3张底牌
        TreeMap<Integer, String> t1 = new TreeMap<>(); //玩家1
        TreeMap<Integer, String> t2 = new TreeMap<>(); //玩家2
        TreeMap<Integer, String> t3 = new TreeMap<>(); //玩家3

        for (int i = 0; i < pokerKeys.size(); i++) {
            //获取键
            Integer key = pokerKeys.get(i); //牌的序号(大小：1~54)
            String value = poker.get(key);  //牌的序号对应的值，牌(花色+点号)
            if (i < pokerKeys.size() - 3) {  //前51张牌发给三个玩家
                if (i % 3 == 0) {
                    t1.put(key, value);
                } else if (i % 3 == 1) {
                    t2.put(key, value);
                } else {
                    t3.put(key, value);
                }
            } else {  //最后三张牌给底牌
                t0.put(key, value);
            }
        }
        //将3张底牌直接发给 玩家1
        t1.putAll(t0);

        //4. 看牌
        t1.forEach((key, value) -> System.out.print(value + "\t"));
        System.out.println();

        t2.forEach((key, value) -> System.out.print(value + "\t"));
        System.out.println();

        t3.forEach((key, value) -> System.out.print(value + "\t"));
        System.out.println();

        t0.forEach((key, value) -> System.out.print(value + "\t"));
    }

}
