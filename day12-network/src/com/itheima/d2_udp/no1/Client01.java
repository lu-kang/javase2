package com.itheima.d2_udp.no1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * 目标：UDP快速入门（客户端）
 */
public class Client01 {
    public static void main(String[] args) throws IOException {

        /*
            编写步骤：
                1、创建DatagramSocket客户端对象，系统会随机分配一个端口号
                2、创建数据包对象封装要发出去的数据。DatagramPacket(byte[] buf, int length, InetAddress address, int port)
                    参数一：封装要发出去的数据。
                    参数二：发送出去的数据大小（字节个数）
                    参数三：服务端的IP地址（找到服务端主机）
                    参数四：服务端程序的端口。
                3、客户端对象开始正式发送数据包，send(DatagramPacket dp)
         */

        // 1、创建客户端对象（发韭菜出去的人）
        DatagramSocket socket = new DatagramSocket(7777);

        // 2、创建数据包对象封装要发出去的数据
        byte[] bytes = "我是快乐的客户端，我爱你abc".getBytes();
        DatagramPacket packet = new DatagramPacket(bytes, bytes.length, InetAddress.getLocalHost(), 6666);

        // 3、客户端对象开始正式发送数据包
        socket.send(packet);

        System.out.println("----客户端数据发送完毕----");
        socket.close(); // 释放资源！
    }
}
