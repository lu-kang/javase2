package com.itheima.d2_udp.no1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * 目标：UDP快速入门（服务端）
 */
public class Server01 {
    public static void main(String[] args) throws IOException {

        /*
            编写步骤：
                1.创建DatagramSocket对象，表示服务端对象，必须指定端口号，和发送端用的要一样
                2.创建数据包对象(用于接收数据)
                3.服务端对象开始使用数据包来接收客户端发来的数据
                4.从数据包中把接收到的数据直接打印出来，接收多少就倒出多少
         */

        System.out.println("----服务端启动----");
        // 1、创建DatagramSocket对象，表示服务端对象，必须指定端口号，和发送端用的要一样
        DatagramSocket socket = new DatagramSocket(6666);

        // 2、创建数据包对象(用于接收数据)
        byte[] buffer = new byte[1024 * 64];
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

        // 3、服务端对象开始使用数据包来接收客户端发来的数据
        socket.receive(packet);

        // 4、从数据包中把接收到的数据直接打印出来，接收多少就倒出多少
        System.out.println(new String(buffer, 0, packet.getLength()));
        System.out.println(packet.getAddress().getHostAddress());
        System.out.println(packet.getPort());

        socket.close(); // 释放资源
    }
}
