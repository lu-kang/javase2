package com.itheima.d2_udp.no2;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

/**
 * 目标：UDP多发多收（客户端）
 */
public class Client02 {
    public static void main(String[] args) throws IOException {
        /*
            需求：通过键盘录入要发送的数据，如果是exit就表示退出，否则就将内容发送给服务器
         */

        // 1、创建客户端对象
        DatagramSocket socket = new DatagramSocket();

        // 2、创建数据包对象封装要发出去的数据
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("请输入：");
            String msg = sc.nextLine();

            // 一旦发现用户输入的exit命令，就退出客户端
            if ("exit".equals(msg)) {
                System.out.println("欢迎下次光临！退出成功！");
                socket.close(); // 释放资源
                break; // 跳出死循环
            }

            byte[] bytes = msg.getBytes();
            DatagramPacket packet = new DatagramPacket(bytes, bytes.length, InetAddress.getLocalHost(), 6666);

            // 3、开始正式发送这个数据包的数据出去了
            socket.send(packet);
        }
    }
}
