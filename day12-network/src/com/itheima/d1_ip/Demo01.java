package com.itheima.d1_ip;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 目标：掌握InetAddress类的常用api使用
 */
public class Demo01 {

    public static void main(String[] args) throws IOException {

        /*
            1、获取InetAddress对象
                static InetAddress getLocalHost()：返回本主机的地址对象
                static InetAddress getByName(String host)：得到指定主机的IP地址对象，参数是域名或者IP地址

            2、调用成员方法
                String getHostName()：获取此IP地址的主机名
                String getHostAddress()：返回IP地址字符串
                boolean isReachable(int timeout)：在指定毫秒内连通该IP地址对应的主机，连通返回true
         */

        // 1、获取本机IP地址对象的
        InetAddress ip1 = InetAddress.getLocalHost();
        System.out.println(ip1.getHostName());
        System.out.println(ip1.getHostAddress());

        // 2、获取指定IP或者域名的IP地址对象。
        InetAddress ip2 = InetAddress.getByName("www.baidu.com");
        System.out.println(ip2.getHostName());
        System.out.println(ip2.getHostAddress());

        // ping www.baidu.com
        System.out.println(ip2.isReachable(6000));

    }
}
