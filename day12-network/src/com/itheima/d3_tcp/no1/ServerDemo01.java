package com.itheima.d3_tcp.no1;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 目标：TCP快速入门（服务端）
 */
public class ServerDemo01 {

    public static void main(String[] args) throws IOException {
        /*
            Socket(String host, int port)：创建客户端，参数为服务端程序的ip和端口
                getOutputStream()：获得字节输出流对象
                getInputStream()：获得字节输入流对象

            ServerSocket(int port)：注册服务端端口
                Socket accept()：等待接收客户端的Socket通信连接，连接成功返回Socket对象与客户端建立端到端通信
         */

        System.out.println("-----服务端启动成功-------");
        // 1、创建ServerSocket的对象，同时为服务端注册端口。
        ServerSocket serverSocket = new ServerSocket(8888);

        // 2、使用serverSocket对象，调用一个accept方法，等待客户端的连接请求
        Socket socket = serverSocket.accept();
        // 3、从socket通信管道中得到一个字节输入流。
        InputStream is = socket.getInputStream();

        // 4、使用输入流读取客户端发送过来的消息
        IOUtils.copy(is, System.out);
        // 其实我们也可以获取客户端的IP地址
        System.out.println(socket.getRemoteSocketAddress());

        is.close();
        socket.close();
    }
}
