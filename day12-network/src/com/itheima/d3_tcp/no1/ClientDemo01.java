package com.itheima.d3_tcp.no1;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * 目标：TCP快速入门（客户端）
 */
public class ClientDemo01 {

    public static void main(String[] args) throws IOException {
        /*
            Socket(String host, int port)：创建客户端，参数为服务端程序的ip和端口
                getOutputStream()：获得字节输出流对象
                getInputStream()：获得字节输入流对象

            ServerSocket(int port)：注册服务端端口
                Socket accept()：等待接收客户端的Socket通信连接，连接成功返回Socket对象与客户端建立端到端通信
         */

        // 1、创建Socket对象，并同时请求与服务端程序的连接。
        Socket socket = new Socket("127.0.0.1", 8888);

        // 2、从socket通信管道中得到一个字节输出流，用来发数据给服务端程序。
        OutputStream os = socket.getOutputStream();
        // 3、开始写数据出去了
        os.write("在一起，好吗？".getBytes());
        os.close();
        socket.close(); // 释放连接资源
    }
}
