package com.itheima.d3_tcp.no2;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 目标：TCP多发多收（服务端）
 */
public class ServerDemo02A {
    public static void main(String[] args) throws IOException {
        System.out.println("-----服务端启动成功-------");
        // 1、创建ServerSocket的对象，同时为服务端注册端口。
        ServerSocket serverSocket = new ServerSocket(8888);

        // 2、使用serverSocket对象，调用一个accept方法，等待客户端的连接请求
        Socket socket = serverSocket.accept();

        // 3、从socket通信管道中得到一个字节输入流。
        InputStream is = socket.getInputStream();

        // 5、使用数据输入流读取客户端发送过来的消息
        IOUtils.copy(is, System.out);
        System.out.println(socket.getRemoteSocketAddress() + "离线了！");
        is.close();
        socket.close();
    }
}
