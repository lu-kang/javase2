package com.itheima.d3_tcp.no2;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 目标：TCP多发多收（服务端）（使用多线程优化）
 */
public class ServerDemo02B {
    public static void main(String[] args) throws IOException {
        System.out.println("--------------服务端启动了（使用线程优化）---------------");
        // 1、创建ServerSocket的对象，同时为服务端注册端口。
        ServerSocket serverSocket = new ServerSocket(8888);

        while (true) {
            // 2、使用serverSocket对象，调用一个accept方法，等待客户端的连接请求
            Socket socket = serverSocket.accept();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        // 3、从socket通信管道中得到一个字节输入流。
                        InputStream is = socket.getInputStream();
                        // 5、使用数据输入流读取客户端发送过来的消息
                        IOUtils.copy(is, System.out);
                        System.out.println(socket.getRemoteSocketAddress() + "离线了！");
                        is.close();
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }
}
