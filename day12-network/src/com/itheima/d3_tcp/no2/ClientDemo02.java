package com.itheima.d3_tcp.no2;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * 目标：TCP多发多收（客户端）
 */
public class ClientDemo02 {
    public static void main(String[] args) throws IOException {
        System.out.println("--------------客户端启动了---------------");
        // 1、创建Socket对象，并同时请求与服务端程序的连接。
        Socket socket = new Socket("127.0.0.1", 8888);

        // 2、从socket通信管道中得到一个字节输出流，用来发数据给服务端程序。
        OutputStream os = socket.getOutputStream();

        // 3、把低级的字节输出流包装成数据输出流
        //DataOutputStream dos = new DataOutputStream(os);

        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("请输入：");
            String msg = sc.nextLine();

            // 一旦用户输入了exit，就退出客户端程序
            if ("exit".equals(msg)) {
                System.out.println("欢迎您下次光临！退出成功！");
                os.close();
                socket.close();
                break;
            }

            // 4、开始写数据出去了
            os.write(msg.getBytes(StandardCharsets.UTF_8));
            os.flush();
        }
    }
}
