package com.itheima.d3_tcp.no3;

import org.apache.commons.io.IOUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 案例：使用TCP通信方式实现文件的上传
 */
public class ServerDemo03A {
    public static void main(String[] args) throws IOException {
        System.out.println("---------服务端启动了-----------");
        //1 创建SocketServer对象，注册端口
        ServerSocket ss = new ServerSocket(8888);

        //2 接收客户端的连接，得到客户端的Socket对象
        Socket socket = ss.accept();

        //3 通过Socket获取字节输入流对象，创建FileOutputStream字节输出流对象
        InputStream is = socket.getInputStream();
        FileOutputStream os = new FileOutputStream("d:/2.jpg");
        //4 将输入流中的内容通过输出流写到硬盘中
        IOUtils.copy(is, os);
        System.out.println("上传成功~");

        //5 释放资源
        os.close();
        socket.close();
    }
}
