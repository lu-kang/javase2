package com.itheima.d3_tcp.no3;

import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

/**
 * 案例：使用TCP通信方式实现文件的上传
 */
public class ClientDemo03 {
    public static void main(String[] args) throws IOException {
        System.out.println("--------客户端启动了---------");

        //1 创建Scanner键盘录入对象，提示并接收用户输入的文件路径
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入要上传的文件路径(包括文件名)：");
        String filePah = sc.nextLine();  //文件路径

        //2 创建FileInputStream字节输入流，读取硬盘中的文件
        FileInputStream fis = new FileInputStream(filePah);

        //3 创建Socket对象，和服务器建立连接
        Socket socket = new Socket("127.0.0.1", 8888);

        //4 通过Socket获取输出流对象，把输入流中的内容写出去
        OutputStream os = socket.getOutputStream();
        // 发送图片
        IOUtils.copy(fis, os);
        System.out.println("上传完成~");
        //5 释放资源
        fis.close();
        socket.close();
    }
}
