package com.itheima.d2_jdk8date;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 目标：掌握日期格式器DateTimeFormatter的使用
 */
public class Demo2 {
    public static void main(String[] args) {
        // 1、创建一个日期时间格式化器对象出来
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        // 2、对时间进行格式化
        LocalDateTime now = LocalDateTime.now();
        String str = formatter.format(now);
        System.out.println(str);

        // 3、解析时间：一般使用LocalDateTime提供的解析方法来解析
        String dateStr = "2029-12-12 12:12:11";
        LocalDateTime ldt = LocalDateTime.parse(dateStr, formatter);
        System.out.println(ldt);
    }
}
