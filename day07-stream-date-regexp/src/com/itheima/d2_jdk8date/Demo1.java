package com.itheima.d2_jdk8date;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 目标：JDK8日期类LocalDateTime、LocalDate、LocalTime常用方法
 */
public class Demo1 {
    public static void main(String[] args) {

        //1、获取日期和时间对象
        LocalDateTime dateTime1 = LocalDateTime.now();
        LocalDateTime ldt = LocalDateTime.of(2029, 12, 12, 12, 12, 12);

        //2、可以获取日期和时间的全部信息（年、月、日、时、分、秒、周）
        int year = ldt.getYear(); // 年
        int month = ldt.getMonthValue(); // 月
        int day = ldt.getDayOfMonth(); // 日
        int hour = ldt.getHour(); //时
        int minute = ldt.getMinute(); //分
        int second = ldt.getSecond(); //秒
        int week = ldt.getDayOfWeek().getValue();  // 周
        int nano = ldt.getNano(); //纳秒

        //3、修改时间：with系列方法 withYear withMonth withDayOfMonth withDayOfYear withHour withMinute withSecond withNano
        LocalDateTime ldt2 = ldt.withYear(2029);
        LocalDateTime ldt3 = ldt.withMinute(59);

        //4、加时间：plus系列方法 plusYears plusMonths plusDays plusWeeks plusHours plusMinutes plusSeconds plusNanos
        LocalDateTime ldt4 = ldt.plusDays(1);
        LocalDateTime ldt5 = ldt.plusMonths(1);

        //5、减时间：minus系列方法 minusDays minusYears minusMonths minusWeeks minusHours minusMinutes minusSeconds minusNanos
        LocalDateTime ldt6 = ldt.minusYears(2);
        LocalDateTime ldt7 = ldt.minusMinutes(3);

        //6、判断2个日期是否相等、在前还是在后：equals、isBefore、isAfter
        System.out.println(ldt6.equals(ldt7));
        System.out.println(ldt7.isAfter(ldt7));
        System.out.println(ldt7.isBefore(ldt7));

        //7、LocalDateTime 转换成 LocalDate、LocalTime
        LocalDate date = dateTime1.toLocalDate();
    }
}
