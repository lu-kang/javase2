package com.itheima.d1_stream;

import java.util.*;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.stream.Collectors;

/**
 * 目标：掌握Stream流中的收集数据到集合或者数组中
 */
public class Demo5 {
    public static void main(String[] args) {
        /*
            Stream流中的收集数据到集合或者数组中
                R collect(Collector c) 把流处理后的结果收集到一个指定的集合中去
                Object[] toArray() 把流处理后的结果收集到一个数组中去

            Collectors工具类提供了具体的收集方式
                public static  Collector toList() 把元素收集到List集合中
                public static  Collector toSet() 把元素收集到Set集合中
                public static  Collector toMap(Function keyMapper , Function valueMapper) 把元素收集到Map集合中
         */

        List<Student> list = List.of(
                new Student("蜘蛛精", 26),
                new Student("紫衣霞", 23),
                new Student("白晶晶", 25),
                new Student("牛魔王", 35),
                new Student("牛夫人", 34)
        );

        // 需求1：请找出年龄大于25的学生对象，并放到一个新集合中去返回。
        List<Student> stus = list.stream()
                .filter(s -> s.getAge() > 25)
                .collect(Collectors.toList());
        System.out.println(stus);

        // 需求2：请找出年龄大于25的学生对象，并把学生对象的名字和年龄，存入到一个Map集合返回。
        Map<String, Integer> map = list.stream()
                .filter(s -> s.getAge() > 25)
                .collect(Collectors.toMap(
                        new Function<Student, String>() {
                            @Override
                            public String apply(Student s) {
                                return s.getName();
                            }
                        }, new Function<Student, Integer>() {
                            @Override
                            public Integer apply(Student s) {
                                return s.getAge();
                            }
                        })
                );
        System.out.println(map);


        // 需求3：请找出年龄大于25的学生对象，存到一个数组中去
        Student[] arr = list.stream()
                .filter(s -> s.getAge() > 25)
                .toArray(Student[]::new);
    }
}
