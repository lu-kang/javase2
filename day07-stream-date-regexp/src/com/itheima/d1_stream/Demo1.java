package com.itheima.d1_stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 目标：分别使用传统方式、Stream API完成下面的需求
 */
public class Demo1 {
    public static void main(String[] args) {
        //创建一个集合，添加若干个元素
        List<String> names = new ArrayList<>();
        names.add("张无忌");
        names.add("周芷若");
        names.add("赵敏");
        names.add("张强");
        names.add("张三丰");

        //需求：找出姓张，且是3个字的名字，存入到一个新集合中去。

        //传统方式
        List<String> list1 = new ArrayList<>();
        for (String name : names) {
            if (name.startsWith("张") && name.length() == 3) {
                list1.add(name);
            }
        }
        System.out.println(list1);

        //Stream流方式
        List<String> list2 = names.stream()
                .filter(s -> s.startsWith("张"))
                .filter(a -> a.length() == 3)
                .collect(Collectors.toList());
        System.out.println(list2);
    }
}
