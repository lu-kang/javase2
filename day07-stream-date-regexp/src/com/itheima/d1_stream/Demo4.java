package com.itheima.d1_stream;

import java.util.*;

/**
 * 目标：掌握Stream流的终结方法
 */
public class Demo4 {
    public static void main(String[] args) {

        /*
            void forEach(Consumer action) 对此流运算后的元素执行遍历
            long count() 统计此流运算后的元素个数
        */

        List<Student> list = List.of(
                new Student("蜘蛛精", 26),
                new Student("蜘蛛精", 26),
                new Student("紫衣霞", 23),
                new Student("白晶晶", 25),
                new Student("牛魔王", 35),
                new Student("牛夫人", 34)
        );

        //需求1：遍历所有学生信息
        list.stream().forEach(s -> System.out.println(s));

        //需求2：请统计年龄大于25的学生有几人
        long count = list.stream()
                .filter(s -> s.getAge() > 25)
                .count();
        System.out.println(count);

    }

}

