package com.itheima.d1_stream;

import java.util.List;
import java.util.stream.Stream;

/**
 * 目标：掌握Stream流的中间方法
 */
public class Demo3 {

    public static void main(String[] args) {
        /*
            Stream filter(Predicate p) 用于对流中的数据进行过滤
            Stream map(Function mapper) 将流中的每个元素转换成另一个元素，map理解为mapping
            Stream limit(long size) 获取前几个元素
            Stream skip(long n) 跳过前几个元素
            Stream distinct() 去除流中重复的元素
            static Stream concat(Stream s1, Stream s2) 合并两个流为一个流
         */

        List<Student> list = List.of(
                new Student("蜘蛛精", 26),
                new Student("蜘蛛精", 26),
                new Student("紫衣霞", 23),
                new Student("白晶晶", 25),
                new Student("牛魔王", 35),
                new Student("牛夫人", 34)
        );

        //需求1：找出年龄大于25的学生，并打印输出
        list.stream()
                .filter(s -> s.getAge() > 25)
                .forEach(n -> System.out.println(n));

        //需求2：找出年龄大于25的学生，获取姓名输出（要求去除重复的名字）
        list.stream()
                .filter(s -> s.getAge() > 25)
                .map(s -> s.getName())
                .distinct()
                .forEach(n -> System.out.println(n));

        //需求3：截取第4到5位的学生
        list.stream()
                .skip(3)
                .limit(2)
                .forEach(s -> System.out.println(s));


        Stream.concat(list.stream(), list.stream()).forEach(s -> System.out.println(s));

    }
}
