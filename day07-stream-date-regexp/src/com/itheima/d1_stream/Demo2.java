package com.itheima.d1_stream;

import java.util.*;
import java.util.stream.Stream;

/**
 * 目标：如何获取List集合、Set集合、Map集合、Array数组的Stream流
 */
public class Demo2 {
    public static void main(String[] args) {

        // 1、如何获取List集合的Stream流
        List<String> list = List.of("张三丰", "张无忌", "周芷若", "赵敏", "张强");
        Stream<String> s1 = list.stream();


        // 2、如何获取Set集合的Stream流
        Set<String> set = Set.of("刘德华", "张曼玉", "蜘蛛精", "马德", "德玛西亚");
        Stream<String> s2 = set.stream();

        // 3、如何获取Map集合的Stream流？
        Map<String, Double> map = Map.of("古力娜扎", 172.3,
                "迪丽热巴", 168.3,
                "马尔扎哈", 166.3,
                "卡尔扎巴", 168.3);

        Stream<Map.Entry<String, Double>> s3 = map.entrySet().stream();


        // 4、如何获取数组的Stream流？
        String[] arr = {"张翠山", "东方不败", "唐大山", "独孤求败"};
        Stream<String> s4 = Arrays.stream(arr);

    }
}
