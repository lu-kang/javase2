package com.itheima.d3_regex;

/**
 * 应用案例：校验字符串（需求：校验用户输入的电话、邮箱是否合法）
 */
public class Demo3 {
    public static void main(String[] args) {
        //需求1：使用正则表达式，校验电话号码（手机|座机）是否合法
        //比如：18676769999、010-34242424、0713-3121699、01034242424、07133121699都是合法的
        System.out.println(checkPhone("1867676999"));  //false
        System.out.println(checkPhone("07133121699"));  //true

        //需求2：使用正则表达式，校验邮箱账号是否合法
        //比如：itheima0009@163.com、itheima@itcast.com.cn、25143242@qq.com 都是合法的
        System.out.println(checkEmail("itheima0009@163.com"));//true
        System.out.println(checkEmail("25143242@qq"));  //false
    }

    public static boolean checkPhone(String tel) {
        return tel.matches("(1[3-9]\\d{9})|(0\\d{2,7}-?[1-9]\\d{4,19})");
    }

    public static boolean checkEmail(String email) {
        return email.matches("\\w{2,}@\\w{2,20}(\\.\\w{2,10}){1,2}");
    }
}
