package com.itheima.d3_regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 应用案例：查找字符串。
 */
public class Demo4 {
    public static void main(String[] args) {

        //需求：请使用正则表达式把下面文本中的电话，邮箱，座机号码，热线都爬取出来。
        String data = """
                来黑马程序员学习Java，
                电话：1866668888，18699997777
                或者联系邮箱：boniu@itcast.cn，
                座机电话：01036517895，010-98951256
                邮箱：bozai@itcast.cn，
                邮箱：dlei0009@163.com，
                热线电话：400-618-9090 ，400-618-4000，4006184000，4006189090
                """;

        // 1、定义爬取规则(正则表达式)
        String regex = "(1[356789]\\d{9})|(\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)|(\\d{3}-?\\d{8}|\\d{4}-?\\d{7})|(400-?\\d{3}-?\\d{4})";

        // 2、把正则表达式封装成一个Pattern对象
        Pattern pattern = Pattern.compile(regex);
        // 3、通过pattern对象去获取查找内容的匹配器对象。
        Matcher matcher = pattern.matcher(data);
        // 4、定义一个循环开始爬取信息
        while (matcher.find()) {
            String rs = matcher.group(); // 获取到了找到的内容了。
            System.out.println(rs);
        }

        //matcher.results().forEach(mr -> System.out.println(mr.group()));
    }
}
