package com.itheima.d3_regex;

/**
 * 目标：演示正则表达式的书写规则
 */
public class Demo2 {
    public static void main(String[] args) {
        // 1、字符类(只能匹配单个字符)
        // [abc] 只能匹配a、b、c  ^如果写在正则表达式的最前面，表示以xx开头
        System.out.println("c".matches("^[abc]"));
        System.out.println("e".matches("[abcd]"));

        //[^abc] 不能是a、b、c
        System.out.println("d".matches("[^abc]"));
        System.out.println("中".matches("[^abc]"));
        System.out.println("a".matches("[^abc]"));

        //[a-zA-Z] 只能是a-z A-Z的字符
        System.out.println("b".matches("[a-zA-Z]"));
        System.out.println("2".matches("[a-zA-Z]"));

        //a到z，除了b和c
        System.out.println("k".matches("[a-z&&[^bc]]"));
        System.out.println("b".matches("[a-z&&[^bc]]"));

        //注意：以上带 [内容] 的规则都只能用于匹配单个字符，不能匹配多个字符
        System.out.println("ab".matches("[a-zA-Z0-9]"));

        System.out.println("=====================================");

        // 2、预定义字符(只能匹配单个字符)  .  \d  \D   \s  \S  \w  \W
        // . 可以匹配任意一个字符
        System.out.println("周".matches(".")); // true
        System.out.println("周日".matches(".")); // false

        // \转义
        System.out.println("转义符：\"\"");

        // \d 表示数字，等价于[0-9]
        System.out.println("3".matches("\\d"));  //true
        System.out.println("a".matches("\\d"));  //false

        // \s 代表一个空白字符
        System.out.println(" ".matches("\\s"));   // true
        System.out.println("a".matches("\\s"));    // false

        // \S 代表一个非空白字符
        System.out.println("a".matches("\\S"));  // true
        System.out.println(" ".matches("\\S")); // false

        //  \w 表示字母、数字、或下划线，等价于[a-zA-Z_0-9]
        System.out.println("a".matches("\\w"));  // true
        System.out.println("_".matches("\\w")); // true
        System.out.println("周".matches("\\w")); // false

        // \W 不是字母数字获取下划线，等价于[^a-zA-Z_0-9]
        System.out.println("周".matches("\\W"));  //true
        System.out.println("a".matches("\\W"));  // false

        System.out.println("23232".matches("\\d")); // false 注意：以上预定义字符都只能匹配单个字符。

        System.out.println("=====================================");

        // 3、数量词： ?代表0次或1次   *代表0次或多次   +代表1次或多次   {n}代表要正好是n次     {n, }代表是>=3次     {n, m}
        System.out.println("a".matches("\\w?"));
        System.out.println("".matches("\\w?"));
        System.out.println("abc".matches("\\w?"));

        System.out.println("abc12".matches("\\w*"));
        System.out.println("".matches("\\w*"));
        System.out.println("abc12周".matches("\\w*"));

        System.out.println("abc12".matches("\\w+"));
        System.out.println("".matches("\\w+"));
        System.out.println("abc12周".matches("\\w+"));

        System.out.println("a3c".matches("\\w{3}"));
        System.out.println("abcd".matches("\\w{3}"));
        System.out.println("abcd".matches("\\w{3,}"));
        System.out.println("ab".matches("\\w{3,}"));
        System.out.println("abcde周".matches("\\w{3,}"));
        System.out.println("abc232d".matches("\\w{3,9}"));

        System.out.println("=====================================");

        // 4、其他几个常用的符号：或：| 、  分组：()
        // 需求1：要求要么是3个小写字母，要么是3个数字。
        System.out.println("abc ".matches("[a-z]{3}|\\d{3}")); // true
        System.out.println("ABC".matches("[a-z]{3}|\\d{3}")); // false
        System.out.println("123".matches("[a-z]{3}|\\d{3}")); // true
        System.out.println("A12".matches("[a-z]{3}|\\d{3}")); // false

        // 需求2：必须是'我爱'开头，中间可以是至少一个'编程'，最后至少是1个'666'
        System.out.println("我爱编程编程666666".matches("我爱(编程)+(666)+"));  //true
        System.out.println("我爱学习编程666".matches("我爱(编程)+(666)+"));  //false
    }
}
