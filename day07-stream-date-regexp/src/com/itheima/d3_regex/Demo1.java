package com.itheima.d3_regex;

/**
 * 目标：体验正则表达式月传统方式
 */
public class Demo1 {
    public static void main(String[] args) {
        //校验QQ号码：不能以0开头，长度必须是6~20位

        System.out.println(checkQQ("0215"));
        System.out.println(checkQQ("21533"));
        System.out.println(checkQQ("21533998"));

        System.out.println("--------------------");

        System.out.println(checkQQByRegExp("0215"));
        System.out.println(checkQQByRegExp("21533"));
        System.out.println(checkQQByRegExp("21533998"));
    }

    //传统方式
    public static boolean checkQQ(String qq) {
        return !qq.startsWith("0") && qq.length() >= 6 && qq.length() <= 20;
    }

    //正则表达式方式
    public static boolean checkQQByRegExp(String qq) {
        return qq.matches("[1-9]\\d{5,19}");
    }
}
