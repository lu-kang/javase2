package com.itheima.d3_regex;

import java.util.Arrays;

/**
 * 目标：String类的替换和切割方法
 */
public class Demo5 {
    public static void main(String[] args) {

        /*
            1、public String replaceAll(String regex , String newStr)：按照正则表达式匹配的内容进行替换
            2、public String[] split(String regex)：按照正则表达式匹配的内容进行分割字符串，反回一个字符串数组。
         */

        // 需求1：请把下面字符串中的不是汉字的部分替换为 “-”
        String s1 = "古力娜扎ai8888迪丽热巴999aa5566马尔扎哈fbbfsfs42425卡尔扎巴";
        System.out.println(s1.replaceAll("\\w+", "-"));

        // 需求2：请把下面字符串中的人名取出来，使用切割来做
        String s2 = "古力娜扎ai8888迪丽热巴999aa5566马尔扎哈fbbfsfs42425卡尔扎巴";
        String[] names = s2.split("\\w+");
        System.out.println(Arrays.toString(names));
    }
}