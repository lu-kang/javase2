package com.itheima.d3_recursion;

/**
 * 目标：递归初体验
 */
public class Demo1 {

    public static void main(String[] args) {
        //递归：从形式上说：方法调用自身的形式称为方法递归（ recursion）。简单说：就是方法自己调自己。
        test1(1);
    }

    public static void test1(int i) { //试着改变 i 的数据类型，对递归的次数有什么影响？
        System.out.println("i = " + i++);
    }
}
