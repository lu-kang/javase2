package com.itheima.d3_recursion;

import java.io.File;

/**
 * 需求：请找出当前模块中所有的Java文件，找到后直接输出其位置。
 */
public class Demo2 {
    public static void main(String[] args) {
        /*
            分析：
                1. 先找出当前模块下的所有一级文件对象
                2. 遍历全部一级文件对象，判断是否是文件
                3. 如果是文件，判断是否是自己想要的Java文件，如果是就输出
                4. 如果是文件夹，需要继续进入到该文件夹，重复上述过程
        */
        File dir = new File("day08-exception-file-recursion-charset");
        findFile(dir);
    }

    //递归查找某个目录下的所有Java文件
    public static void findFile(File dir) {  //File要表示文件夹对象
        //1 先找出当前模块下的所有一级文件对象
        File[] files = dir.listFiles();
        //2 遍历全部一级文件对象，判断是否是文件
        for (File file : files) {  //file有可能是文件，有可能是文件夹
            //判断是否是文件
            if (file.isFile()) {
                //3 如果是文件，判断是否是自己想要的Java文件，如果是就输出
                if (file.getName().endsWith(".java")) {  //file.getName()获取文件名
                    System.out.println(file);
                }
            } else {
                //4 如果是文件夹，需要继续进入到该文件夹，重复上述过程
                findFile(file);
            }
        }
    }
}
