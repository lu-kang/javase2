package com.itheima.d2_file;

import java.io.File;

/**
 * 目标：File类构造器
 */
public class Demo1 {
    public static void main(String[] args) {
        /*
            File封装的对象仅仅是一个文件(夹)的路径名，这个路径可以是存在的，也可以是不存在的。
                public File (String pathname) 根据文件路径创建文件对象
                public File (String parent, String child) 根据父路径和子路径名字创建文件对象
                public File (File  parent, String child) 根据父路径对应文件对象和子路径名字创建文件对象

            路径的写法：
                  绝对路径：以盘符开头的路径：例如：D:/develop/a.txt
                  相对路径：不以盘符开头,相对当前project项目目录，默认从当前项目（project）中查找文件
         */

        // 1、创建一个File对象，指代某个具体的文件。
        File f1 = new File("D:/resource/a.txt");
        System.out.println(f1.length()); // 文件大小，如果文件不存在返回0

        File f2 = new File("D:/resource");
        System.out.println(f2.length());

        // 注意：File对象可以指代一个不存在的文件路径
        File f3 = new File("D:/resource/ab.txt");
        System.out.println(f3.length());
        System.out.println(f3.exists()); // false

        // 我现在要定位的文件是在模块中，应该怎么定位呢？
        // 绝对路径：带盘符的
        // 相对路径（重点）：不带盘符，默认是直接去工程下寻找文件的。
        File f4 = new File("D:\\JavaSE2\\day08-exception-file-recursion-charset\\src\\com\\itheima\\d2_file\\Demo1.java");
        File f5 = new File("day08-exception-file-recursion-charset\\src\\com\\itheima\\d2_file\\Demo1.java");
        System.out.println(f4.length());
        System.out.println(f5.length());
    }
}
