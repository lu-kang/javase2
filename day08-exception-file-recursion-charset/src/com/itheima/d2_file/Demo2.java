package com.itheima.d2_file;

import java.io.File;

/**
 * 目标：File类判断、获取文件信息的方法
 */
public class Demo2 {
    public static void main(String[] args) {

        /*
            1、public boolean exists() 判断当前文件对象，对应的文件路径是否存在，存在返回true
            2、public boolean isFile() 判断当前文件对象指代的是否是文件，是文件返回true，反之返回false
            3、public boolean isDirectory() 判断当前文件对象指代的是否是文件夹，是文件夹返回true，反之返回false。
            4、public String getName() 获取文件的名称（包含后缀）
            5、public long length() 获取文件的大小，返回字节个数
            6、public long lastModified() 获取文件的最后修改时间。
            7、public String getPath() 获取创建文件对象时，使用的路径
            8、public getAbsolutePath() 获取绝对路径
         */

        File f1 = new File("D:/resource/ab.txt");

        System.out.println(f1.exists());
        System.out.println(f1.isFile());
        System.out.println(f1.isDirectory());
        System.out.println(f1.getName());
        System.out.println(f1.length());
        System.out.println(f1.lastModified());
        System.out.println(f1.getPath());
        System.out.println(f1.getAbsolutePath());
    }
}
