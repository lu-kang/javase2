package com.itheima.d2_file;

import java.io.File;
import java.io.IOException;

/**
 * 目标：File类创建、删除方法
 */
public class Demo3 {
    public static void main(String[] args) throws IOException {
        /*
            1、public boolean createNewFile()：创建一个新文件（文件内容为空），创建成功返回true,反之。
            2、public boolean mkdir()：用于创建文件夹，注意：只能创建一级文件夹
            3、public boolean mkdirs()：用于创建文件夹，注意：可以创建多级文件夹
            4、public boolean delete()：删除文件，或者空文件，注意：不能删除非空文件夹。
         */

        File f1 = new File("D:/resource/itheima2.txt");
        f1.createNewFile();

        File f2 = new File("D:/resource/aaa");
        f2.mkdir();

        File f3 = new File("D:/resource/bbb/ccc/ddd/eee/fff/ggg");
        f3.mkdirs();

        System.out.println(f1.delete());
        System.out.println(f2.delete());

        File f4 = new File("D:/resource");
        System.out.println(f4.delete());
    }
}
