package com.itheima.d2_file;

import java.io.File;
import java.util.Arrays;

/**
 * 目标：File类遍历文件夹的方法
 */
public class Demo4 {
    public static void main(String[] args) {
        /*
            1、public String[] list() 获取当前目录下所有的"一级文件名称"到一个字符串数组中去返回。
            2、public File[] listFiles() 获取当前目录下所有的"一级文件对象"（重点）

            注意事项：
                1.当主调是文件时，或者路径不存在时，返回null
                2.当主调是空文件夹时，返回一个长度为0的数组
                3.当主调是一个有内容的文件夹时，将里面所有一级文件和文件夹路径放在File数组中，并把数组返回
                4.当主调是一个文件夹，且里面有隐藏文件时，将里面所有文件和文件夹的路径放在FIle数组中，包含隐藏文件
                5.当主调是一个文件夹，但是没有权限访问时，返回null
         */

        File f1 = new File("D:\\course\\待研发内容");
        String[] names = f1.list();
        for (String name : names) {
            System.out.println(name);
        }

        File[] files = f1.listFiles();
        System.out.println(Arrays.toString(files));
        for (File file : files) {
            System.out.println(file.getAbsolutePath());
        }
    }
}
