package com.itheima.d4_charset;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 * 目标：字符集的编码、解码操作
 */
public class Demo1 {

    public static void main(String[] args) throws UnsupportedEncodingException {

        /*
            编码：
                byte[] getBytes() 使用平台的默认字符集将该 String编码为一系列字节，将结果存储到新的字节数组中
                byte[] getBytes(String charsetName) 使用指定的字符集将该 String编码为一系列字节，将结果存储到新的字节数组中
            解码：
                String (byte[] bytes) 通过使用平台的默认字符集解码指定的字节数组来构造新的 String
                String (byte[] bytes, String charsetName) 通过指定的字符集解码指定的字节数组来构造新的 String

            注意：编解码使用的字符集必须一样，否则出现乱码。英文和数字不会乱码，因为其他字符集都兼容ASCII字符集
         */

        String data = "你A啊";

        // 1、编码：将字符串转换成字节数组
        byte[] bytes1 = data.getBytes(); // 默认是按照平台字符集（UTF-8）进行编码的
        byte[] bytes2 = data.getBytes("GBK");// 按照指定字符集进行编码
        System.out.println(Arrays.toString(bytes1));
        System.out.println(Arrays.toString(bytes2));

        // 2、解码：将字节数组转换成字符串
        String s1 = new String(bytes1); // 按照平台默认编码（UTF-8）解码
        String s2 = new String(bytes2, "GBK");// 按照指定字符集进行编码

        System.out.println(s1);
        System.out.println(s2);
    }


}
