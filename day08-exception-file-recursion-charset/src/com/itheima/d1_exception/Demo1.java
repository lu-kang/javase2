package com.itheima.d1_exception;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 目标：认识异常、异常的继承体系
 */
public class Demo1 {

    public static void main(String[] args) throws ParseException {
        test1();
        test2();
    }

    public static void test1() {
        int[] arr = {10, 20, 30};
        System.out.println(arr[3]);
    }

    public static void test2() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.parse("1999/09/09");
    }
}
