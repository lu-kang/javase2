package com.itheima.d1_exception;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 目标：异常的处理方式（2种方案）
 */
public class Demo2 {

    public static void main(String[] args) throws ParseException {
        test1();
        test2();
    }

    public static void test1() throws ParseException {
        //方案1：抛出异常（throws）：在方法上使用throws关键字把方法内部出现的异常抛出去给调用者处理。
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.parse("1999/09/09");
    }

    public static void test2() {
        //方案2：捕获异常(try…catch)：使用try…catch捕获了异常，也就意味着处理了异常，程序不会终止。
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            sdf.parse("1999/09/09");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
