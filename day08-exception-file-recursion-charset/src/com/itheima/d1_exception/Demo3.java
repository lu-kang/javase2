package com.itheima.d1_exception;

/**
 * 目标：自定义异常、抛出编译时异常和运行时异常的区别
 */
public class Demo3 {
    public static void main(String[] args) throws MyExceptionA {
        /*
            需求：写一个saveAge(int age)方法，在方法中对参数age进行判断，
                如果age<0或者>=150就认为年龄不合法，如果年龄不合法，就给调用者抛出一个年龄非法异常。
         */

        saveAge1(80);
        saveAge2(-80);
    }

    public static void saveAge1(int age) throws MyExceptionA {
        //年龄不合法，抛出MyException表示年龄不合法
        if (age < 0 || age > 150) {
            // throw 抛出去这个异常对象
            throw new MyExceptionA();
        }
        System.out.println("年龄保存成功：" + age);
    }

    public static void saveAge2(int age) {
        //年龄不合法，抛出MyException表示年龄不合法
        if (age < 0 || age > 150) {
            // throw 抛出去这个异常对象
            throw new MyExceptionB();
        }
        System.out.println("年龄保存成功：" + age);
    }
}
