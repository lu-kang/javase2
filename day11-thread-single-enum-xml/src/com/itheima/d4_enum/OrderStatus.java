package com.itheima.d4_enum;

import javax.management.Descriptor;

//订单状态枚举类
public enum OrderStatus {

    //枚举项
    NO_PAY(1, "未支付"),
    YES_PAY(2, "已支付"),
    COMPLETED(3, "已完成"),
    CANCEL(4, "已取消");


    OrderStatus(Integer status, String msg) {
    }

}
