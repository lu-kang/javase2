package com.itheima.d4_enum;

/**
 * 目标：掌握枚举类的定义
 * 格式：修饰符 enum 枚举类名{ 枚举类实例对象名; }
 */
public class Demo01 {
    public static void main(String[] args) {
        OrderStatus orderStatus = OrderStatus.NO_PAY;
        System.out.println(orderStatus);
        System.out.println(orderStatus.ordinal());
    }

}

