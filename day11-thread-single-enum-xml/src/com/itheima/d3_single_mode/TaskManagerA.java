package com.itheima.d3_single_mode;

//饿汉式单例设计模式
public class TaskManagerA {

    private TaskManagerA() {
    }

    private static TaskManagerA taskManager = new TaskManagerA();

    public static TaskManagerA getInstance() {
        return taskManager;
    }
}
