package com.itheima.d3_single_mode;

/**
 * 目标：掌握单例设计模式
 */
public class Demo01 {
    public static void main(String[] args) {

    /*
        饿汉式单例设计模式的实现步骤
             1.将构造器私有
             2.定义一个本类类型的静态属性，并且new一个本类对象进行赋值
             3.定义一个getInstance()方法，返回上面创建的对象

        懒汉单例设计模式的实现步骤
             1.将构造器私有
             2.定义一个本类类型的静态属性不需要初始化
             3.定义一个getInstance()方法，判断本类对象是否存在，不存在就创建本类对象返回，存在就直接返回。

        需求1：使用饿汉单例设计模式，设计一个任务管理器类TaskManagerA
        需求2：使用懒汉单例设计模式，设计一个任务管理器类TaskManagerB
     */


    }
}

