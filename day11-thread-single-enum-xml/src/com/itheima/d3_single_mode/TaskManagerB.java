package com.itheima.d3_single_mode;

//懒汉单例设计模式
public class TaskManagerB {

    private TaskManagerB() {
    }

    private static TaskManagerB taskManager;

    public static TaskManagerB getInstance() {
        //判断本类对象是否存在
        if (taskManager == null) {
            //不存在就创建本类对象
            taskManager = new TaskManagerB();
        }
        return taskManager;
    }
}
