package com.itheima.d5_xml;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.util.List;

/**
 * 目标：掌握dom4j解析xml文件（了解，API不用记背）
 */
public class Demo02 {
    public static void main(String[] args) throws DocumentException {
        /*
            dom4j：获取 SAXReader解析器、Document文档、Element根标签对象的API：
                 public SAXReader()：创建Dom4j的解析器对象
                 Document read(String path)：加载xml文件成为Document对象
                 Element getRootElement()：获得根元素对象

            dom4j：解析xml标签、属性、文本的API：
                 List<Element> elements()：得到当前元素下所有子元素
                 List<Element> elements(String name)：得到当前元素下指定名字的子元素返回集合
                 Element element(String name)：得到当前元素下指定名字的子元素,如果有很多名字相同的返回第一个
                 String getName()：得到元素名字
                 String  attributeValue(String name)：通过属性名直接得到属性值
                 String elementText(子元素名)：得到指定名称的子元素的文本
                 String getText()：得到文本

            需求：读取students.xml文件中的数据，获取每一个student标签下name标签、age标签、address标签、id属性值
         */


        // 1、创建一个Dom4J框架提供的解析器对象
        SAXReader saxReader = new SAXReader();
        // 2、使用saxReader对象把需要解析的XML文件读成一个Document对象。
        Document document = saxReader.read("day11-thread-single-enum-xml\\students.xml");
        // 3、从文档对象中解析XML文件的全部数据了
        Element root = document.getRootElement();

        // 4、获取根元素下的全部student子元素
        List<Element> elements = root.elements("student");
        for (Element element : elements) {
            String id = element.attributeValue("id");
            String name = element.element("name").getText();
            String age = element.element("age").getText();
            String address = element.element("address").getText();

            System.out.println(id + ", " + name + ", " + age + ", " + address);
        }
    }
}
