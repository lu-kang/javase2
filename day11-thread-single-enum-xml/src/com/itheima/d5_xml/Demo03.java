package com.itheima.d5_xml;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 案例：利用dom4J框架，将students.xml文件中的联系人数据，解析出来，封装成List集合，并遍历输出。
 */
public class Demo03 {

    public static void main(String[] args) throws DocumentException {

        // 1、创建一个Dom4J框架提供的解析器对象
        SAXReader saxReader = new SAXReader();
        // 2、使用saxReader对象把需要解析的XML文件读成一个Document对象。
        Document document = saxReader.read("day11-thread-single-enum-xml\\students.xml");
        // 3、从文档对象中解析XML文件的全部数据了
        Element root = document.getRootElement();

        // 4、获取根元素下的全部student子元素
        List<Element> elements = root.elements("student");

        List<Student> list = elements.stream().map(element -> {
            //获取student标签下name标签、age标签、address标签、id属性值
            String id = element.attributeValue("id");
            String name = element.element("name").getText();
            String age = element.element("age").getText();
            String address = element.element("address").getText();

            return new Student(Integer.parseInt(id), name, Integer.parseInt(age), address);
        }).collect(Collectors.toList());

        list.forEach(s -> System.out.println(s));
    }
}
