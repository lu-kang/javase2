package com.itheima.d5_xml;

/**
 * 目标：掌握创建XML文件，了解基本语法规则
 */
public class Demo01 {
    public static void main(String[] args) {
        /*
            需求：在当前模块下创建一个students.xml文件，存储3个学生数据
                   姓名 name：张三              姓名 name：李四                 姓名 name：王五
                   学号 id：1                  学号 id：2                     学号 id：3
                   年龄 age：23                年龄 age：24                   年龄 age：25
                   地址 address：武汉           地址 address：成都             地址 address：西安

            要求：学生数据可以使用student标签存储，其中学号id可以使用属性进行存储
         */

    }
}
