package com.itheima.d2_thread_pool;

import java.util.concurrent.*;

/**
 * 目标：掌握创建线程池（2种方式）
 */
public class Demo01 {
    public static void main(String[] args) {
        /*
            方式1：线程池接口ExecutorService，而这个接口下有一个实现类叫ThreadPoolExecutor类，可以用来创建线程池对象。
                ThreadPoolExecutor构造器7个参数：
                    核心线程数，
                    最大线程数，
                    临时线程最大存活时间，
                    存活时间的单位，
                    任务队列（等待的任务），
                    创建线程的线程工厂（Executors.defaultThreadFactory()），
                    拒绝策略（默认策略：new ThreadPoolExecutor.AbortPolicy() 丢弃任务并抛出RejectedExecutionException异常）

            方式2：线程池工具类Executors
                Executors.newFixedThreadPool(int nThreads)：创建固定线程数量的线程池，如果某个线程因为执行异常而结束，那么线程池会补充一个新线程替代它
         */

        ExecutorService pool1 = new ThreadPoolExecutor(
                3,    //核心线程数有3个
                5,  //最大线程数有5个。   临时线程数=最大线程数-核心线程数=5-3=2
                10,    //临时线程存活的时间10秒。 意思是临时线程10秒没有任务执行，就会被销毁掉。
                TimeUnit.SECONDS,//时间单位（秒）
                new ArrayBlockingQueue<>(4), //任务阻塞队列，没有来得及执行的任务在，任务队列中等待
                Executors.defaultThreadFactory(), //用于创建线程的工厂对象
                new ThreadPoolExecutor.AbortPolicy() //拒绝策略
        );


        // 1、通过Executors创建一个线程池对象。
        ExecutorService pool2 = Executors.newFixedThreadPool(9);
        //核心线程数量到底配置多少呢？？？ 核心线程数量 = CPU的核数 + 1 | CPU核数 * 2

    }
}

