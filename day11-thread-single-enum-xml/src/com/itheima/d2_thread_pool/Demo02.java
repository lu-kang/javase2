package com.itheima.d2_thread_pool;

import java.util.concurrent.*;

/**
 * 目标：掌握线程池处理Runnable任务、Callable任务
 */
public class Demo02 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        /*
            ExecutorService的常用api：
                 execute(Runnable runnable)：执行任务/命令，没有返回值，一般用来执行 Runnable 任务
                 Future<T> submit(Callable<T> task)：执行任务，返回未来任务对象获取线程结果，一般拿来执行Callable任务
                 shutdown()：等任务执行完毕后关闭线程池
         */

        ExecutorService pool = Executors.newFixedThreadPool(9);

        pool.submit(() -> {
            System.out.println("runnable线程任务");
        });


        pool.submit(() -> {
            System.out.println("callable线程任务");
            return "ok";
        });

        pool.shutdown();
    }
}