package com.itheima.d2_thread_pool;

import java.util.concurrent.*;

/**
 * 目标：了解线程池的拒绝策略
 */
public class Demo03 {

    public static void main(String[] args) {
        /*
            拒绝策略：
                 new ThreadPoolExecutor.AbortPolicy：丢弃任务并抛出RejectedExecutionException异常
                 new ThreadPoolExecutor.DiscardPolicy：丢弃任务，但是不抛出异常
                 new ThreadPoolExecutor.DiscardOldestPolicy：抛弃队列中等待最久的任务 然后把当前任务加入队列中
                 new ThreadPoolExecutor.CallerRunsPolicy：由主线程负责调用任务的run()方法从而绕过线程池直接执行
         */

        ExecutorService pool = new ThreadPoolExecutor(
                3,    //核心线程数有3个
                5,  //最大线程数有5个。   临时线程数=最大线程数-核心线程数=5-3=2
                10,    //临时线程存活的时间10秒。 意思是临时线程10秒没有任务执行，就会被销毁掉。
                TimeUnit.SECONDS,//时间单位（秒）
                new ArrayBlockingQueue<>(10), //任务阻塞队列，没有来得及执行的任务在，任务队列中等待
                Executors.defaultThreadFactory(), //用于创建线程的工厂对象
                new ThreadPoolExecutor.DiscardPolicy() //拒绝策略
        );

        for (int i = 1; i <= 10; i++) {

            int a = i;
            pool.execute(() -> {
                System.out.println(Thread.currentThread().getName() + " 执行第 " + a + " 个任务");
                while (true) {

                }
            });
        }

    }
}

