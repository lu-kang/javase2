package com.itheima.d2_thread_pool;

/**
 * 目标：掌握线程的生命周期
 */
public class Demo04 {
    public static void main(String[] args) throws InterruptedException {

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("2. " + Thread.currentThread().getState());
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        System.out.println("1. " + t.getState());

        t.start();

        //t.stop();

        Thread.sleep(100);

        System.out.println("3. " + t.getState());
    }
}
