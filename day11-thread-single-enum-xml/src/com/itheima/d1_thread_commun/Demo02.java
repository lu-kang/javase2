package com.itheima.d1_thread_commun;

public class Demo02 {

    private static Object lock = new Object();

    public static void main(String[] args) throws InterruptedException {

        //创建10个线程
        for (int i = 0; i < 10; i++) {
            Thread.sleep(50);
            String threadName = Integer.toString(i);
            new Thread(() -> {
                synchronized (lock) {
                    String cthreadName = Thread.currentThread().getName();
                    System.out.println("线程 [" + cthreadName + "] 正在等待.");
                    try {
                        lock.wait();
                        Thread.sleep(50);
                        System.out.println("线程 [" + cthreadName + "] 被唤醒了.");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }, threadName).start();
        }

        Thread.sleep(1000);

        synchronized (lock) {
            lock.notify();
            lock.notify();
        }

    }
}