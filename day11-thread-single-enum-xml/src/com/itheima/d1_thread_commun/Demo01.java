package com.itheima.d1_thread_commun;

/**
 * 目标：了解线程间通信的实现方式
 */
public class Demo01 {

    private static Object lock = new Object();

    public static void main(String[] args) {
        /*
            等待和唤醒方法：
                wait()：让当前线程等待并释放所占锁，直到另一个线程调用notify()方法或 notifyAll()方法
                notify()：唤醒正在等待的单个线程
                notifyAll()：唤醒正在等待的所有线程

            需求：一个线程负责做包子，一个线程负责吃包子。做一个吃一个。
         */


        //1. 生产包子
        new Thread(() -> {
            while (true) {
                synchronized (lock) {
                    try {
                        System.out.println("生产一个包子");
                        Thread.sleep(1000);
                        lock.notify();
                        lock.wait();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        //2. 消费包子
        new Thread(new Runnable() {
            @Override
            public void run() {
                //一直生产包子
                while (true) {
                    synchronized (lock) {
                        try {
                            System.out.println("吃了一个包子");
                            Thread.sleep(1000);
                            lock.notify();
                            lock.wait();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }
}