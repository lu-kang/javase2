package com.itheima.d2_object;

/**
 * Object类概述：是所有类的父类，任何一个类的对象(包括数组)，都可以调用Object类的方法
 */
public class Demo {
    public static void main(String[] args) {
        Student s1 = new Student("张三", 18);
        Student s2 = new Student("张三", 18);

        //需求：打印对象的时候不期望看到地址值，希望时对象的属性值。
        System.out.println(s1);
        System.out.println(s2);

        //需求：比较s1和s2是否相等，如果两个对象的所有属性值都相等，我们就认为两个对象相等
        System.out.println(s1.equals(s2));
    }
}
