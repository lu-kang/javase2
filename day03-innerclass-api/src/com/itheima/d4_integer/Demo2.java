package com.itheima.d4_integer;

/**
 * 目标：包装类对基本类型和字符串数据进行相互转换
 */
public class Demo2 {
    public static void main(String[] args) {
        /*
            1.字符转换为其他的基本类型数据
                需求1：把"12345"转换为12345
                需求2：把"12.345"转换为12.345
                需求3：把"true"转换为true
         */
        int num1 = Integer.parseInt("12345");
        double num2 = Double.parseDouble("12.345");
        boolean num3 = Boolean.parseBoolean("true");

        /*
            2.把基本数据类型转换为字符串
                需求1：把12345转换为"12345"
                需求2：把12.345转换为“12.345”
                需求3：把true转换为“true"
         */
        String s1 = String.valueOf(12345);
        String s2 = String.valueOf(12.345);
        String s3 = String.valueOf(true);
    }
}
