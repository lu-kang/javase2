package com.itheima.d4_integer;

/**
 * 案例：求下面字符串中每一个整数的和
 * String str = "100,200,300,50,60,75,250";
 */
public class Demo3 {
    public static void main(String[] args) {
        String str = "100,200,300,50,60,75,250";

        String[] arr = str.split(",");

        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            int num = Integer.parseInt(arr[i]);
            sum += num;
        }
        System.out.println(sum);

    }
}
