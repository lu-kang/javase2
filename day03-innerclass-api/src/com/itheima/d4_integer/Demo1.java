package com.itheima.d4_integer;

/**
 * 目标：基本类型 和 包装类 相互转换
 * <p>
 * 自动装箱：指的是把基本类型转换为包装类
 * 自动拆箱：指的是把包装类转换为基本类型
 */
public class Demo1 {
    public static void main(String[] args) {
        //需求1：将基本类型 包装为 包装类

        //需求2：将包装类 拆箱为 基本类型

    }
}
