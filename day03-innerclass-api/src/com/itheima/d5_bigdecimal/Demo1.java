package com.itheima.d5_bigdecimal;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 目标：掌握BigDecimal的四则运算
 */
public class Demo1 {
    public static void main(String[] args) {
        //问题：小数在进行运算时，由于底层存储原理，有精度损失问题.
        System.out.println(0.09 + 0.01);//0.09999999999999999
        System.out.println(1.301 / 100);//0.013009999999999999

        BigDecimal d1 = BigDecimal.valueOf(0.09);
        BigDecimal d2 = BigDecimal.valueOf(0.03);

        //需求：使用BigDecimal对0.09和0.01进行四则运算
        //1.求和 add
        System.out.println(d1.add(d2));
        //2.求差 subtract
        System.out.println(d1.subtract(d2));
        //3.乘法 multiply
        System.out.println(d1.multiply(d2));
        //4.除法 divide
        System.out.println(d1.divide(d2));
        //System.out.println(d1.divide(d2, 2, RoundingMode.HALF_UP));
    }
}
