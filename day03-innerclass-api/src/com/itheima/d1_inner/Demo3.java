package com.itheima.d1_inner;

import java.util.*;
import java.util.function.Consumer;

/**
 * 目标：掌握匿名内部类作为参数传递的应用场景
 */
public class Demo3 {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("大雄");
        list.add("静香");
        list.add("胖虎");
        list.add("小夫");

        // 调用forEach方法，观察参数是什么类型，传递对应的匿名内部类对象。
        list.forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        });

    }
}
