package com.itheima.d1_inner.demo2;

/**
 * 目标：掌握匿名内部类
 */
public class Demo2 {
    public static void main(String[] args) {

        Animal animal = new Animal() {
            @Override
            public void eat() {
                System.out.println("吃东西");
            }
        };
    }
}
