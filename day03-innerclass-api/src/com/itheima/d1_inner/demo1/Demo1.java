package com.itheima.d1_inner.demo1;

/**
 * 目标：了解成员内部类
 * 目标：了解静态内部类
 * 目标：了解局部内部类
 */
public class Demo1 {

    public static void main(String[] args) {
        Outer.InnerA innerA = new Outer().new InnerA();

        Outer.InnerB innerB = new Outer.InnerB();


    }

}
