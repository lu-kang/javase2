package com.itheima.d1_inner.demo1;

public class Outer {

    class InnerA {
        private String name;
    }

    static class InnerB {
        private String name;
    }


    public void m1() {
        class C {
        }

        C c = new C();
    }

}
