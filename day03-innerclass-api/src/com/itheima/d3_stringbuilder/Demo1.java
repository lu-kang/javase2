package com.itheima.d3_stringbuilder;

/**
 * 目标：掌握StringBuilder的常用方法
 * <p>
 * StringBuilder是一个字符串容器，容器中的内容可以发生改变，对容器操作效率高
 */
public class Demo1 {
    public static void main(String[] args) {
        /*
            public StringBuilder()
            public StringBuilder(String s)

            public StringBuilder append(String s): 往StringBuilder容器中添加元素，并返回StringBuilder容器本身
            public StringBuilder reverse(): 将StringBuilder容器中的内容反转，并返回StringBuilder容器本身
            public StringBuilder toString(): 将StringBuilder转换为String
         */

        StringBuilder sb = new StringBuilder();
        sb.append("黑马");
        sb.append("程序员");
        System.out.println(sb.toString());
        System.out.println(sb.reverse());
        System.out.println(sb.toString());

    }
}
