package com.itheima.d3_stringbuilder;

/**
 * StringBuilder案例：设计一个方法，按照指定格式返回数组内容，要求返回的数组内容格式如：：[11, 22, 33]
 */
public class Demo2 {
    public static void main(String[] args) {
        int[] arr = {11, 22, 33, 44, 55};
        System.out.println(printArray(arr));
    }

    public static String printArray(int[] arr) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < arr.length; i++) {
            if (i == arr.length - 1) {
                sb.append(arr[i]);
            } else {
                sb.append(arr[i]).append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}