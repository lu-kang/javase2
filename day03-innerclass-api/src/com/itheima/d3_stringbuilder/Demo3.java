package com.itheima.d3_stringbuilder;

/**
 * StringBuilder案例：设计一个方法，用于判断字符串是否对称，如果对称返回true,否则返回false. 如：abc不是对称的，aba是对称的
 */
public class Demo3 {
    public static void main(String[] args) {
        System.out.println(isReverseEquals("abc"));
    }

    public static boolean isReverseEquals(String str) {
        StringBuilder sb = new StringBuilder(str);
        sb.reverse();
        return str.equals(sb.toString());
    }
}
