package com.itheima.d6_thread_safe;

/**
 * 目标：掌握线程安全
 */
public class Demo01 {

    public static void main(String[] args) {

        /*
            需求：某电影院目前正在上映国产大片，目前只剩下1张票，而有2个窗口在售卖。模拟2个窗口同时卖票

            分析：
                (1)定义一个类SellTicket实现Runnable接口，里面定义一个成员变量：private int tickets = 1;
                (2)在SellTicket类中重写run()方法实现卖票，判断票数大于0，就卖票，并告知是哪个窗口卖的
                (3)卖了票之后，票数要减1
                (4)定义一个测试类Demo，里面有main方法，创建SellTicket类的对象
                (5)创建2个Thread类的对象，把SellTicket对象作为构造方法的参数，并给出对应的窗口名称
                (6)启动线程
         */

        //创建SellTicket类的对象
        SellTicket st = new SellTicket();

        //创建2个Thread类的对象，把SellTicket对象作为构造方法的参数，并给出对应的窗口名称
        Thread t1 = new Thread(st, "窗口1");
        Thread t2 = new Thread(st, "窗口2");

        //启动线程
        t1.start();
        t2.start();
    }
}

