package com.itheima.d6_thread_safe;

public class SellTicket implements Runnable {

    private int ticket = 1;

    //在SellTicket类中重写run()方法实现卖票，代码步骤如下
    @Override
    public void run() {
        if (ticket > 0) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ticket--;
            System.out.println(Thread.currentThread().getName() + "在卖票, 还剩下" + ticket + "张票");
        }
    }

}