package com.itheima.d3_commons_io;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;

/**
 * 目标：掌握commons-io工具包
 */
public class Demo01 {
    public static void main(String[] args) throws IOException {
        /*
            commons-io工具包：
                commons-io是apache开源基金组织提供的一组有关IO操作的工具类，可以提高IO功能开发的效率
                commons-io提供了很多有关io操作的类。有两个主要的工具类FileUtils, IOUtils

            FileUtils工具类：
                String readFileToString(File file, String charset)：读取文件中的数据, 返回字符串
                void copyFile(File srcFile, File destFile)：复制文件
                void copyDirectory(File srcDir, File destDir)：复制文件夹

            IOUtils工具类：
                int copy(Reader reader, Writer writer)：将输入流拷贝到输出流
                int copy(InputStream input, OutputStream output)：将输入流拷贝到输出流

            需求：使用commons-io简化复制文件
         */


        //1.FileUtils
        FileUtils.copyFile(new File("d:/a.txt"), new File("d:/c.txt"));

        //2.IOUtils
        IOUtils.copy(new FileInputStream("d:/a.txt"), new FileOutputStream("d:/c.txt"));
    }
}
