package com.itheima.d4_thread_create;

/**
 * 目标：掌握创建线程的方式2：实现Runnable接口（推荐方式）
 */
public class Demo02 {
    public static void main(String[] args) {

        /*
            需求：定义一个线程任务类，去实现Runnable接口，重写run方法。开启线程使用

            实现步骤：
                1. 通过Thread类的有参构造Runnable创建线程
                2. 实现Runnable接口的run方法
                3. 调用线程对象的start()方法，启动线程
         */

        //1. 通过Thread类的有参构造Runnable创建线程
        //2. 实现Runnable接口的run方法
        Thread thread = new Thread(() -> System.out.println("线程2"));
        //3. 调用线程对象的start()方法，启动线程
        thread.start();
        System.out.println("主线程");
    }
}
