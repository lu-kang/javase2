package com.itheima.d4_thread_create;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 目标：掌握创建线程的方式3：实现Callable接口
 */
public class Demo03 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        /*
            实现步骤：
                1. 创建一个类Callable接口的匿名内部类，重写call方法，封装要做的事情，和要返回的数据。
                2. Callable类型的对象封装成FutureTask（线程任务对象）。
                3. 创建Thread对象，把FutureTask线程任务对象交给Thread对象。
                4. 调用Thread对象的start方法启动线程。
                5. 线程执行完毕后，通过FutureTask对象的的get方法去获取线程任务执行的结果。


            需求：定义一个线程任务类，实现Callable接口
         */

        //1. 创建一个类Callable接口的匿名内部类，重写call方法，封装要做的事情，和要返回的数据。
        Callable<String> callable = () -> {
            System.out.println("线程3");
            return "ok";
        };
        //2. Callable类型的对象封装成FutureTask（线程任务对象）。
        FutureTask<String> futureTask = new FutureTask<>(callable);
        //3. 创建Thread对象，把FutureTask线程任务对象交给Thread对象。
        Thread thread = new Thread(futureTask);
        //4. 调用Thread对象的start方法启动线程。
        thread.start();
        System.out.println("主线程");
        //5. 线程执行完毕后，通过FutureTask对象的的get方法去获取线程任务执行的结果。
        System.out.println(futureTask.get());

    }
}
