package com.itheima.d4_thread_create;

/**
 * 目标：掌握线程创建的方式1：继承Thread
 */
public class Demo01 {

    public static void main(String[] args) {

        /*
            需求：定义一个线程类，重写run方法。开启线程使用

            实现步骤：
                1. 定义一个MyThread类继承线程类java.lang.Thread，重写run()方法
                2. 创建MyThread类的对象
                3. 调用线程对象的start()方法启动线程（启动后自动执行run()方法）
         */

        //1. 定义一个MyThread类继承线程类java.lang.Thread，重写run()方法
        //2. 创建MyThread类的对象
        MyThread myThread = new MyThread();
        //3. 调用线程对象的start()方法启动线程（启动后自动执行run()方法）
        myThread.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("主线程");

    }
}
