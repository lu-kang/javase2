package com.itheima.d1_serialize_io;

import java.io.*;

/**
 * 目标：掌握对象字节流
 */
public class Demo01 {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        /*
            ObjectInputStream：
                ObjectOutputStream(OutputStream out)：把原始字节输出流包装成高级的对象字节输出流
                void writeObject(Object obj)：把对象写出去到对象序列化流的文件中去。注意：被写入对象的类必须实现Serializable接口

            ObjectInputStream：
                ObjectInputStream(InputStream out)：把低级字节输如流包装成高级的对象字节输入流
                Object readObject()：把存储到磁盘文件中去的对象数据恢复成内存中的对象返回。注意：读取对象的顺序要和写入的顺序保持一致

            需求：创建一个Teacher对象和一个Student对象，使用对象序列化流把对象存保存到文件中，再读取出来。

            注意：读到文件末尾不是返回null，而是抛出了一个EOFException异常。EOF：End Of File。为了不终止程序，所有要try...catch捕获处理。
         */

        //1. 创建Student对象
        Student stu = new Student("张三", 23);
        Teacher tea = new Teacher("李四", 25);
        //2. 把对象序列化到文件中，创建对象序列化流
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("d:/a.txt"));
        oos.writeObject(stu);
        oos.writeObject(tea);
        //3.释放资源
        oos.close();

        //6.反序列化，就是把文件中的对象再拿到内存中来
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("d:/a.txt"));
        Student s = (Student) ois.readObject();
        Teacher t = (Teacher) ois.readObject();
        //ois.readObject();
        System.out.println(s);
        System.out.println(t);
        ois.close();

    }
}
