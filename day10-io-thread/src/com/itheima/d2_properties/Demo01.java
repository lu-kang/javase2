package com.itheima.d2_properties;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * 目标：掌握Properties对象的使用
 */
public class Demo01 {
    public static void main(String[] args) throws IOException {
        /*
            构造器：
                public Properties()：用于构建Properties集合对象（空容器）

            方法：
                load(InputStream input)：从输入字节流读取属性列表（键和元素对）
                load(Reader reader)：从输入字符流读取属性列表（键和元素对）
                String getProperty(String key)：使用此属性列表中指定的键搜索属性值 (get)
         */

        //1.创建Properties对象
        Properties prop = new Properties();

        //2.调用load方法加载属性文件,将数据读取到Properties对象中
        prop.load(new FileInputStream("d:/a.properties"));

        //3.从Properties对象中获取键和值
        System.out.println(prop.getProperty("zs"));
    }
}
