package com.itheima.d2_properties;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * 目标：掌握Properties对象的使用（了解）
 */
public class Demo02 {

    public static void main(String[] args) throws IOException {
        /*
            构造器：
                public Properties()：用于构建Properties集合对象（空容器）

            方法：
                store(OutputStream out, String comments)：将此属性列表（键和元素对）写入此 Properties表中，以适合于使用 load(InputStream)方法的格式写入输出字节流
                store(Writer writer, String comments)：将此属性列表（键和元素对）写入此 Properties表中，以适合使用 load(Reader)方法的格式写入输出字符流
                Object setProperty(String key, String value)：保存键值对（put）
         */

        //1.创建Properties对象
        Properties prop = new Properties();

        //2.调用load方法加载属性文件,将数据读取到Properties对象中
        prop.load(new FileInputStream("d:/a.properties"));

        prop.setProperty("wb", "22");
        prop.store(new FileOutputStream("d:/b.properties"), null);

        //3.从Properties对象中获取键和值
        System.out.println(prop.getProperty("zs"));

    }
}
