package com.itheima.d7_thread_sync;

import java.util.concurrent.locks.ReentrantLock;

public class SellTicket implements Runnable {

    private int ticket = 1;

    private final ReentrantLock lock = new ReentrantLock();


    //在SellTicket类中重写run()方法实现卖票，代码步骤如下
    @Override
    public void run() {
        lock.lock();
        if (ticket > 0) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ticket--;
            System.out.println(Thread.currentThread().getName() + "在卖票, 还剩下" + ticket + "张票");
        }
        lock.unlock();
    }
}