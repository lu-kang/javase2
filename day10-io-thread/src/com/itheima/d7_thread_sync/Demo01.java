package com.itheima.d7_thread_sync;

/**
 * 目标：掌握线程同步方案（3种）
 */
public class Demo01 {

    public static void main(String[] args) {

        /*
            方案1：同步代码块
                格式：public void run() { synchronized ( 同步锁对象 ) { 操作共享资源的代码 } }
                原理：每次只能一个线程进入代码块，执行完毕后自动解锁，其他线程才可以进来执行

            方案2：同步方法
                格式：public synchronized void run() { 方法体 }
                原理：每次只能一个线程进入方法，执行完毕以后自动解锁，其他线程才可以进来执行。

            方案3：Lock锁
                格式：Lock是接口不能直接实例化，这里采用它的实现类 ReentrantLock 来构建Lock锁对象。
                    ReentrantLock lock = new ReentrantLock();
                原理：
                    lock.lock(); 获得锁
                    lock.unlock(); 释放锁

            需求：使用同步代码块改进窗口卖票案例
         */

        //创建SellTicket类的对象
        SellTicket st = new SellTicket();

        //创建2个Thread类的对象，把SellTicket对象作为构造方法的参数，并给出对应的窗口名称
        Thread t1 = new Thread(st, "窗口1");
        Thread t2 = new Thread(st, "窗口2");

        //启动线程
        t1.start();
        t2.start();
    }
}
