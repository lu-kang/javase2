package com.itheima.d5_thread_api;

/**
 * 目标：掌握线程常用方法（上）
 */
public class Demo01 {

    public static void main(String[] args) throws InterruptedException {

        /*
            构造器：
                Thread(String name)：可以为当前线程指定名称
                Thread(Runnable target ，String name )：封装Runnable对象成为线程对象，并指定线程名称

            线程相关方法：
                String getName()：获取当前线程的名称，默认线程名称是Thread-索引
                void setName(String name)：将此线程的名称更改为指定的名称，通过构造器也可以设置线程名称
                static Thread currentThread()：获取当前线程对象
         */

        Thread thread = new Thread(() -> System.out.println("线程2"), "小张的线程");
        thread.start();

        System.out.println(thread.getName());

        Thread t = Thread.currentThread();
        System.out.println(t.getName());

    }
}

