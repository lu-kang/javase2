package com.itheima.d5_thread_api;

/**
 * 目标：掌握线程常用方法（下）
 */
public class Demo02 {
    public static void main(String[] args) throws InterruptedException {

        /*
            构造器：
                Thread(String name)：可以为当前线程指定名称
                Thread(Runnable target ，String name )：封装Runnable对象成为线程对象，并指定线程名称

            线程相关方法：
                void sleep(long time)：让当前线程休眠指定的时间后再继续执行，单位为毫秒
                void join()：让调用当前这个方法的线程先执行完

            需求：完成10秒倒计时的效果
         */

        Thread t1 = new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("小张：" + i);
            }
        }, "小张的线程");

        Thread t2 = new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("小李：" + i);
            }
        }, "小李的线程");

        t1.start();
        //t1.join();
        t2.start();
    }
}
